<?php

class AdminPagesCest {

    /**
     * @dataProvider pageProvider
     */
    public function loggedInPages(\FunctionalTester $I, \Codeception\Example $example) {
        $I->amLoggedInAs(1);

        if (isset($example['params'])) {
            $I->amOnRoute($example['route'], $example['params']);
        } else {
            $I->amOnRoute($example['route']);
        }

        $I->expectTo('see admin page ' . $example['title']);
        $I->see($example['title'], '#main h1');
    }

    /**
     * @dataProvider pageProvider
     */
    public function notLoggedInPages(\FunctionalTester $I, \Codeception\Example $example) {
        $I->stopFollowingRedirects();

        if (isset($example['params'])) {
            $I->amOnRoute($example['route'], $example['params']);
        } else {
            $I->amOnRoute($example['route']);
        }

        $I->expectTo('be redirected to the login page');
        $I->seeResponseCodeIsRedirection();
    }

    protected function pageProvider() {
        return [
            ["route" => "/usm/default/index", "title" => "Ultimate Site Management"],
            ["route" => "/usm/menu", "title" => "Menu"],
            ["route" => "/usm/menu/create", "title" => "Create new menu item"],
            ["route" => "/usm/menu/update", "params" => ["id" => 1], "title" => "Edit menu item: Home"],
            ["route" => "/usm/pages", "title" => "Pages"],
            ["route" => "/usm/pages/create", "title" => "Create new page"],
            ["route" => "/usm/pages/update", "params" => ["id" => 1], "title" => "Edit page: Home"],
            // @example(url="/usm/page/2", title=""
            ["route" => "/usm/files", "title" => "Files"],
            ["route" => "/usm/files/create", "title" => "Upload new file"],
            // @example(url="/usm/files/update?id=1", title=""
            // @example(url="/file/v2/69d084e2f18b5aba4bcc.xlsx", title=""
            ["route" => "/usm/textblocks", "title" => "Textblocks"],
            ["route" => "/usm/textblocks/create", "title" => "Create new textblock"],
            // @example(url="/usm/textblocks/update?id=1", title=""*/
            ["route" => "/usm/redirects", "title" => "URLs"],
            ["route" => "/usm/redirects/create", "title" => "Create new URL"],
            ["route" => "/usm/redirects/update", "params" => ["id" => 2], "title" => "Edit URL: /"],
            ["route" => "/usm/default/configuration", "title" => "Website configuration"],
            ["route" => "/usm/default/kitchensink", "title" => "Kitchen sink"],
        ];

        /* POST
            /usm/pages/delete?id=2
            /usm/menu/delete?id=100
            /usm/files/delete?id=1
            /usm/textblocks/delete?id=1
            /usm/redirects/delete?id=2 
        */
    }
}
