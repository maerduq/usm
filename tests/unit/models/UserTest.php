<?php

namespace tests\unit\models;

use maerduq\usm\models\User;

class UserTest extends \Codeception\Test\Unit {
    public function testFindUserById() {
        verify($user = User::getAdmin())->notEmpty();
        verify($user->username)->equals('admin');

        verify(User::findIdentity(999))->empty();
    }

    public function testFindUserByUsername() {
        verify($user = User::findByUsername('admin'))->notEmpty();
        verify(User::findByUsername('not-admin'))->empty();
    }

    /**
     * @depends testFindUserByUsername
     */
    public function testValidateUser() {
        $user = User::getAdmin();

        verify($user->validatePassword('hoihoi'))->notEmpty();
        verify($user->validatePassword('123456'))->empty();
    }
}
