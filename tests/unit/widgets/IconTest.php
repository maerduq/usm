<?php

namespace tests\unit\widgets;

use maerduq\usm\widgets\IconWidget;
use Yii;

class IconTest extends \Codeception\Test\Unit {
    public function testEyeIcon() {
        $renderingResult = IconWidget::widget(['icon' => 'eye']);

        verify($renderingResult)->stringContainsString('bi');
        verify($renderingResult)->stringContainsString('bi-eye');

        verify($renderingResult)->stringNotContainsString('getver');
    }

    public function testCustomClass() {
        $renderingResult = IconWidget::widget(['icon' => 'eye', 'options' => ['class' => 'custom-class']]);

        verify($renderingResult)->stringContainsString('bi');
        verify($renderingResult)->stringContainsString('bi-eye');
        verify($renderingResult)->stringContainsString('custom-class');

        verify($renderingResult)->stringNotContainsString('getver');
    }
}
