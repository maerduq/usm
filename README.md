# Ultimate Site Management

Published under [MIT license](LICENSE).

## Quick links

- [📣 Release notes](docs/release-notes.md)
- [Contributing](docs/CONTRIBUTING.md)

## About

With Ultimate Site Management (USM) you can transform any Yii2 project into an easy manageable website! If you are a Developer that wants to enable less tech-savvy users to manage the website, USM is for you!

USM is a _Content Management System_ for [Yii2](https://www.yiiframework.com/). It is specifically designed with the situation in mind where a **developer** makes and supports a Yii2 based website, and a **user** should be given tools to maintain the content of the website.

The developer sets up all the dynamic content, database models and whatnot, and he uses the tools USM gives him, like [Pages](https://docs.depaul.nl/usm/admin-panel/pages), [Textblocks](docs/features/textblocks.md) and [Files](docs/features/files.md), to hand over some basic maintenance abilities to the less tech savvy user.

The user is given a web interface called the [Admin Panel](https://docs.depaul.nl/usm/admin-panel/overview). On this Admin Panel, the user can edit (parts of) pages, files, or specific content developed by the developer to be maintained by the user.

Using [Plugins](docs/plugins/introduction), the developer can even develop specific modules and expose the required maintenance tools via the admin panel to the user.

### Features of USM

- Menu administration with drag-and-drop interface.
- WYSIWYG editor for complete pages, or for certain parts of web pages by using textblocks.
- HTML editing for more advanced users.
- Multilingual support.
- Plugin support.
- Easy hooks to incorporate CMS objects in your own pages.
- Upload and manage files.

## Documentation

- [USM User Manual](https://docs.depaul.nl/usm/)
- [USM Developer Manual](docs/README.md)

## Contributors

- Paul Marcelis
- Joseph Verburg
