<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=database;dbname=app_test',
    'username' => 'root',
    'password' => 'password',
    'charset' => 'utf8mb4',
];
