<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

/**
 * Application configuration shared by all test types
 */
return [
    'id' => 'basic-tests',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['usm'],
    'aliases' => [
        '@vendor' => __DIR__ . '/../../vendor',
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@usm'   => __DIR__ . '/../../src'
    ],
    'language' => 'en-US',
    'modules' => [
        'usm' => [
            'class' => \maerduq\usm\UsmModule::class,
            'access_type' => 'usm2',
            'access_password' => '$2y$13$qsL8/BJOj5s5wtWwsQrSUO3oNIYQCW.gEYerqsxu5YWIlv9wBLQSW',
        ],
    ],
    'components' => [
        'db' => $db,
        'mailer' => [
            'class' => \yii\symfonymailer\Mailer::class,
            'viewPath' => '@app/mail',
            // send all mails to a file by default.
            'useFileTransport' => true,
            'messageClass' => 'yii\symfonymailer\Message'
        ],
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
        'urlManager' => [
            'showScriptName' => true,
        ],
        'user' => [
            'identityClass' => 'maerduq\usm\models\User',
            'loginUrl' => ['/usm/global/login'],
        ],
        'request' => [
            'cookieValidationKey' => 'test',
            'enableCsrfValidation' => false,
            // but if you absolutely need it set cookie domain to localhost
            /*
            'csrfCookie' => [
                'domain' => 'localhost',
            ],
            */
        ],
    ],
    'params' => $params,
];
