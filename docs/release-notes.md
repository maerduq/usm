# 📣 Release notes

See what has changed in USM.

## v3.0.0 @ 18 September 2023

> Quite some breaking changes! We have an [upgrade guide](setup/upgrade-guide.md#upgrading-from-v2x-to-v3x) for you!

## Added

- Added a kitchensink view at `/usm/default/kitchensink` to see all widgets etc.
- Added the developer documentation to the repository!

## Changed

- Now using the `yii2-bootstrap5` package, which was long overdue. **This is not compatible with `yii2-bootstrap`, so your project should move to yii2-bootstrap5 as well.** There is a simple [migration guide](https://github.com/yiisoft/yii2-bootstrap5/blob/master/docs/guide/migrating-yii2-bootstrap.md) to help you with that.
- Implemented the USM user with the user component now. That way, being logged in via USM shows as being really logged in for the Yii framework. When using the [access_type](setup/configuration-options.md#access_type) `usm` or `usm2`, you should edit the component in your `web.php` config as following:

_config/web.php_

```
...
'components' => [
    'user' => [
        'identityClass' => 'maerduq\usm\models\User',
        'loginUrl' => ['/usm/global/login'],
    ],
    ...
]
...
```

- Renamed `maerduq\usm\assets\MaerduqBootstrapAsset` to `maerduq\usm\assets\BootstrapAsset`.
- Removed `maerduq\usm\assets\ModalWidget`, because the bootstrap5 Modal widget is okay now.
  - **Please use `yii\bootstrap5\Modal` instead**.
- Removed `maerduq\usm\assets\GlyphiconWidget`, because glyphicons are not part of Bootstrap 5.
  - **Please use `maerduq\usm\assets\IconWidget` instead**.
- Removed `maerduq\usm\assets\BootstrapIconsAsset`, because there is an asset bundle provided by yii2-bootstrap5.
  - **Please use `yii\bootstrap5\BootstrapIconAsset` instead**.
- BootstrapIcons are now implemented via the `.bi` css class instead of the SVG. Be sure any styling you applied to icons rendered by `IconWidget` is changed accordingly.
- Tweaked the admin panel styling to use more Bootstrap 5 styling.
- Added the developer documentation to the source code!

## Fixed

- Fixed rendering an admin page that has no title or page header.
- Fixed properly setting the access level for the page for which you create a new menu item.
- Fixed quite some code styling.
- Fixed proper access management to USM login and logout pages.
- Fixed some Dutch translations.
- Only use USM error page when user is admin, else the user gets an ugly meta-error page.

## v2.1.6 @ 25 March 2023

#### Fixed

- Make sure USM works with PHP 8.1 (Thanks _Reinier van der Leer_)
- Made some changes to make sure unit testing works.

## v2.1.2 @ 7 August 2022

#### Added

- The USM login url now also accepts a returnURL as GET parameter.

#### Fixed

- A model using an USM File can now be deleted if the file is optional, or if the file is configured with onDelete RESTRICTED.

## v2.1.0 @ 5 August 2022

#### Added

- WysiwygTextarea can now be used [as ActiveField widget](features/wysiwyg-editing.md#use-as-activefield-widget).

## v2.0.0 @ 3 August 2022

#### Added

- Textblocks can now be edited inline by users! For this, textblocks should be integrated using the [Textblock widget](features/textblocks.md#textblock-widget) instead of through the Textblocks model's _read()_ function.
- A universal [Edit mode ](features/wysiwyg-editing.md#edit-mode)has been introduced.
- An _intro_ layout block has been introduced for Admin pages from plugins.
- New configuration option [_availablePagesForMenu_](setup/configuration-options.md#availablepagesformenu) to present normal application pages as page available for in the menu that are not from a plugin.
- It is now easy to [integrate a USM File into your own Model](features/files.md).
- A very handy [WysiwygTextarea widget ](features/wysiwyg-editing.md#wysiwygtextarea-widget)has been introduced.
- Files now have a new URL to reference them, which is less guessable and has the file extension in it.
- We added an error page in USM admin panel style when an error occurs in the admin panel.
- When a Plugin page or Yii controller linked with a menu item does not explicitly set a page title, the page title is taken from the menu item title.

#### Changed

- The website configuration can now be found on a separate admin panel page instead on the admin panel homepage.
- The _last accessed_ date of a file is now not influenced by viewing the file from the admin panel.
- The _updated_ date of a file is now not influenced by viewing the file.
- The admin panel style is ported to SASS.

#### Fixed

- Activation of menu item linked to a Plugin page now works.
- The USM toolbar doesn't influence the style of the website itself anymore.
- Deleting a page that has previously been linked to a menu item could not be deleted in some cases. This is fixed now.
- If the files directory is not writable, files will still be readable now.

## v1.7.1 @ 1 February 2022

#### Fixed

- Make sure the bootstrap assets are not loaded by the USM toolbar.

## v1.7.0 @ 18 December 2021

#### Added

- Add two formatters to built-in formatter helper: `asUl` and `asHyperlink`.

#### Changed

- Change built-in login page, removing it from admin layout.
- Change text editor behavior with regard to empty lines.
- Move all documentation text from readme.md to [docs.depaul.nl/usm](http://docs.depaul.nl/usm).

#### Fixed

- Fixed some more Dutch translations.

## v1.6.1 @ 9 November 2021

#### Changed

- Removed setting breadcrumbs in cms page view file, since this overwrote the breadcrumb generation in Usm::getBreadcrumbs().

#### Fixed

- Fixed "editMode not found" javascript error.
- Fixed UsmModule references in RedirectRule (now you should really bootstrap usm in your config files!).
- Fixed cms page link generation for lastPageWithLink=true.
- Fixed setting the default layout.

## v1.6.0 @ 11 October 2021

> Execute the following steps in order to use this version to its fullest:
>
> 1. run the USM migrations
> 2. Add the USM module to the _bootstrap_ configuration of both your `web.php` and `console.php` configuration files.
> 3. add the USM module to your `config/console.php`
> 4. add `<?= Usm::renderToolbar($this) ?>` to the body of your layout.
> 5. if you are using the `usm` access_type, switch to `usm2`

#### Added

- Started with translations for the admin panel to Dutch.
- Added `Usm::renderToolbar($this)` hook for in your layout view to be able to render the USM toolbar on every page.
- USM toolbar can now be minified.
- Introduced a more secure `usm2` access_type configuration, to remove the need of an plaintext password in your `config/web.php`.
- Introduced a brute force detection and prevention mechanism to the USM default login page.
- Added logging of USM login attempts to a new USM database table.
- Added more filter and sorting options to the URLs edit page in the admin panel.
- Added some more information texts to the admin panel home page.

#### Changed

- Changed default style Bootstrap v3 to Maerduq Bootstrap in the admin panel.
- Restyled the USM Toolbar to a real bar at the bottom of the page.
- Moved the items of the top-right secondary menu in the admin panel to the side menu.

## v1.5.0 @ 1 November 2020

> Possible breaking changes for HTML editing your pages. The new editor does not support the "hack" anymore to close HTML tags that were not opened in the page content.

#### Changed

- Upgraded the experience of editing pages! We now use a different library (Jodit instead of TinyMCE and Codemirror), making it possible to solve raw HTML editing and WYSIWYG editing in one flow! You can now easily edit and save your page content from the USM Toolbar.

![](.assets/v1.5.0-usm-toolbar.png)

- Editing page properties is now done through a modal opened in the USM Toolbar instead of a separate admin page to improve page editing flow.
- In editing page properties the "WYSIWYG editing" flag has been replaced by the option "Edit as raw HTML".
- Updated the editor library for textblock content editing

#### Fixed

- Fixed default empty layout to contain basic headers.
- Fixed CSRF in default empty and container layouts.
- URL creation for USM pages and files with additional query parameters.

## v1.4.3 @ 3 October 2020

#### Fixed

- Fixed saving a file with a long mime type, like Microsoft Office files.

## v1.4.2 @ 27 September 2020

#### Changed

- Styling for smaller screens is optimized.

## v1.4.1

#### Changed

- Make title input form for base language a normal ActiveField, so that error summary could be removed from menu item form.

## v1.4.0

> New migrations!

#### Added

- Introduced `UsmPluginModule.php` to enable [module as plugin](plugins/introduction.md#module-as-plugin-since-v-1-4-0).
- Introduced the **Plugin page** destination type for menu items and URLs.

#### Changed

- Parse URL of created and updated URLs to lower case and remove characters with special accents.
- After updating a menu item, stay on the page.
- When chooseing a menu item as destination for your URL, the options are now in order of menu position.
- Client validation of menu item form has been enabled.

## v1.3.2

#### Fixed

- Fixed the mapping of submenu's to its parent in `Usm::getMenu()`.

## v1.3.1

#### Fixed

- Fixed setting `$this->params['pageHeader']` when wysiwyg editing a page.

## v1.3.0

> New admin panel layout!

#### Added

- Introduced `maerduq\usm\widgets\IconWidget` to include Bootstrap Icons in views.
- Added application configuration insights to admin panel home page.
- Added sorting and filtering to all overview tables.
- Don't allow URLs starting with `usm/` anymore, because this breaks the admin panel.

#### Changed

- Admin panel now has a new style with a sidebar layout.

![](.assets/newstyle.png)

- Replaced JQuery UI with Dragula as drag-and-drop library for the menu item ordering.
- All admin panel scripts and styles are put in AssetBundles.
- All dependencies have been updated.
- Updated all used icons in admin panel to Bootstrap Icons.
- Updated admin panel texts.
- Improved implementation of `maerduq\usm\components\TranslatedActiveRecord`and the way the models with translations handle reading and updating translations.
- Upgraded all admin panel views.
- Added sorting and filtering to all overview tables.
- _Add new file_ is now a new page instead of a roll down panel in the file overview admin page.
- Added some more rules to models.

#### Fixed

- Consistent `k&r` code formatting.
- Fixed redirect URL for links.
- Handling a link redirect is now done more nicely with a 302 status code.
- Removed unused classes `UsmPlugin`, `UsmPluginController`, and `SessionController`.

## v1.2.1

#### Fixed

- Fixed parsing admin menu items of plugins with an url containing only the action ID.

## v1.2.0

#### Changed

- Updated readme.
- Don't silently ignore anymore when a plugin could not be loaded.
- Changed handling when USM migrations were not run from a die() to throwing an exception.

#### Fixed

- Removed unused file `src/Test.php`.
