# Upgrade guide

Planning to upgrade a USM based project to a new major version? This will guide you through the process.

## Upgrading from v2 to v3

### Significant impact
* You will update to USM v3.x.
* You will update to bootstrap5. If you have been using an older version of bootstrap in your website, you will experience some visual changes that you will need to address.

### Upgrading to USM Starter 3 boilerplate

- Copy-paste the `deploy`, `script-dev-setup`, and `script-dev-watch` scripts from [the USM Starter source files](https://bitbucket.org/maerduq/usm-starter/src).
- Make sure the following configuration is in place for the `"setPermission"` action in `composer.json`

```
"runtime": "0777",
"web/assets": "0777",
"files/usm": "0777",
"yii": "0755",
"deploy": "0755",
"script-dev-setup": "0755",
"script-dev-watch": "0755"
```

- Copy-paste the new `docker/Dockerfile` and the new `docker-compose.yml` from [the USM Starter source files](https://bitbucket.org/maerduq/usm-starter/src/).
- Set the `allowedIPs` array for gii and debug in your web and console config files as following, to allow for all Docker IP addresses:

```
'allowedIPs' => ['127.0.0.1', '::1', '172.*.0.1', '192.168.*.1'],
```

- (Optional) rewrite some string-based class references to the ::class-style reference. Example:
	Old: `'yii\caching\FileCache'`
	New: `\yii\caching\FileCache::class`


### Upgrading Bootstrap 3 to Bootstrap 5

> This is required to upgrade to USM 3. If you use Bootstrap for your own website as well, Bootstrap 3 and Bootstrap 5 cannot be used in the same project. You need to migrate to Bootstrap 5 for your own website as well.

- Replace in `composer.json`:

```
"yiisoft/yii2-bootstrap": "^2.0",
```
By
```
"yiisoft/yii2-bootstrap5": "~2.0.2",
```

- Make sure all instances of `yii\bootstrap\` are replaced by `yii\bootstrap5\` in your project. It could be as easy as find-replace, but to be sure, use the [migration guide of Yii2 Bootstrap5](https://github.com/yiisoft/yii2-bootstrap5/blob/master/docs/guide/migrating-yii2-bootstrap.md).
- Add `yii\bootstrap5\i18n\TranslationBootstrap` to the bootstrap configuration of your `config/web.php`.
- Fix your bootstrap styling. Some pointers:
  - Replace helper class `text-right` with `text-end`
  - Start using margin and padding helper classes
- Fix your icons
	- Make sure the icons that previously used `GlyphiconWidget` use an icon name that's available in [Bootstrap Icons](https://icons.getbootstrap.com/).
	- Fix the style applied to icons rendered with `IconWidget`. This should not be applied to an `svg`, but to an `i` tag. The `bi` class is still available.


### Upgrading to USM 3

- Replace in `composer.json`:

```
"maerduq/usm": "^2.0",
```
By
```
"maerduq/usm": "^3.0",
```

- When using `usm` or `usm2` USM access type, make sure the `user` component in `config/web.php` is configured as following:

```
'user' => [
	'identityClass' => \maerduq\usm\models\User::class,
	'loginUrl' => ['/usm/global/login'],
]
```

- Replace the following dependencies in your code:
	- `maerduq\usm\assets\MaerduqBootstrapAsset` -> `maerduq\usm\assets\BootstrapAsset`
	- `maerduq\usm\assets\ModalWidget` -> `yii\bootstrap5\Modal`
	- `maerduq\usm\assets\GlyphiconWidget` -> `maerduq\usm\assets\IconWidget`
	- `maerduq\usm\assets\BootstrapIconsAsset` -> `yii\bootstrap5\BootstrapIconAsset`


### Upgrading swiftmailer to symfonymailer

This is according to the Yii2 basic application. Swiftmailer is deprecated.

- Replace in `composer.json`:

```
"yiisoft/yii2-swiftmailer": "^2.0",
```
By
```
"yiisoft/yii2-symfonymailer": "~2.0.3",
```

- Add to `.env` and `.env.example` the following:

```
SENDER_EMAIL="noreply@example.com"
SENDER_NAME="Example"
MAIL_HOST=mailpit
MAIL_PORT=1025
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTED=0
MAIL_TO_FILE=0
```

- Copy-paste the new `config/params.php` from [the USM Starter source files](https://bitbucket.org/maerduq/usm-starter/src/master/config/).
- Make sure you update your mailer component configuration in `config/web.php` to the following:

```
'mailer' => [
	'class' => \yii\symfonymailer\Mailer::class,
	'transport' => [
		'dsn' => ($_ENV['MAIL_ENCRYPTED'] ? 'smtps' : 'smtp') .'://' . $_ENV['MAIL_USERNAME'] . ':' . $_ENV['MAIL_PASSWORD'] . '@' . $_ENV['MAIL_HOST'] . ':' . $_ENV['MAIL_PORT'],
	],
	'viewPath' => '@app/mail',
	'useFileTransport' => $_ENV['MAIL_TO_FILE'] ? true : false,
],
```

- Copy-paste the `html.php` and `text.php` mail layout files from [the USM Starter source files](https://bitbucket.org/maerduq/usm-starter/src/master/mail/layouts).


### Final actions

- Run `# composer update`. Please note that this will also update your other dependencies.
- Run `# bash script-dev-setup` and make sure it works.
- Now check your website again with your tests or manually.
