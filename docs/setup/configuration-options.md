# Configuration options

When defining the USM module in your `config/web.php` file, there are several
configuration options.

## Overview

- [access_type](#access_type) (required) - How should user access to the admin panel be checked.
- [access_password](#access_password) - Password when using access type _usm_ or _usm2_.
- [access_admin_check](#access_admin_check) - For access type _yii_, the function to return a boolean expressing whether a user should be able to access the admin panel.
- [availablePagesForMenu](#availablepagesformenu) - (since v2.0) register plugin pages from your base app.
- _languages_ - List of languages to be supported by USM. The first language should be your primary language as set in `config/web.php`. If not, USM will prepend this language to the language array.
  - Type: array
  - Example: `['nl', 'en']`
- _layout_container_ - Path to the main layout where simple pages should be put in.
  - Type: string
  - Example: `/layouts/main`
- _layout_plain_ - Path to the layout without page container.
  - Type: string
- _layout_empty_ - Path to the layout for pages that require no layout.
- [plugins](#plugins) - List of controllers or modules containing USM plugins.
- [sitemaps](#sitemaps) - List of controllers that provide a partial sitemap.

## Usage

You can access the USM configuration in your code using:

`UsmModule::getInstance()->{configItem}`

## Options

### access_type

Options:

- `usm` (deprecated, use `usm2` instead)
- `usm2`
- `yii`

#### usm (deprecated)

A simple login system. Put the admin password in plaintext in [access_password](#access_password). Users will be prompted for the password when trying to access the USM admin panel or any page with an access level above "All visitors".

#### usm2

Second iteration of the `usm` access type. Requires a hashed password in the [access_password](#access_password) configuration option. Access check is the same as for `usm`.

When using this access type, make sure the user component in your web configuration is set to the USM user model (since v3.0.0):

_config/web.php_

```
...
'components' => [
    'user' => [
        'identityClass' => 'maerduq\usm\models\User',
        'loginUrl' => ['/usm/global/login'],
    ],
    ...
]
...
```

#### yii

Use the default user access system of Yii through the `Yii::$app->user` object.

Make sure to define the [access_admin_check](#access_admin_check) configuration option with a function that returns whether the current user may access the USM admin panel and pages labeled with access level _Administrators_.

### access_password

Value: `string`

For access_type `usm`, this field should contain the plaintext admin password.

For access_type `usm2`, this field should contain the hashed admin password. Use the following command to generate the hash for your password:

```
$ php yii usm/pwtool/hash <youradminpassword>
```

Make sure USM is defined as a module in `config/console.php` for this to work.

### access_admin_check

Value: `function`

This function should return a boolean, expressing whether the current user is an admin or not. The following code example shows such a function that uses the `admin` property of the user identity in order to determine wether the current user is an admin

_Example_

```php
'access_admin_check' => function () {
    if (Yii::$app->user->isGuest) {
        return false;
    }
    return (Yii::$app->user->identity->admin);
},
...
```

### availablePagesForMenu

List of Plugin pages provided by the base app that can be used in the menu item editor to link to menu items.

Should be an array of items with a _name_ and an _actionId_, as shown below

```php
'availablePagesForMenu' => [
  ['name' => 'Demo page', 'actionId' => '/site/demo'],
  ..
]
```

### plugins

Allows you to register controllers and/or modules as USM plugin. The value should be an array containing values as following:

- Controller ID of controllers extending _UsmController_, formatted as Yii style `{controllerId}` or `/{moduleId}/{controllerId}`.
- Module ID of modules extending _UsmPluginModule_, formatted in Yii style, like `{moduleId}` or `{moduleId}/{submoduleId}`.

[Learn more about plugins and how they work.](../plugins/introduction.md)

#### Example

In the following example, the module _admin_ and controller _test_ are configured as USM plugin. The _admin_ module provides two plugins: _clients_ and _projects_. This all results in three USM admin pages and one USM plugin page:

The config file defines the USM module with plugins and the admin module that is configured as plugin.

_controllers/TestController.php_

```php
<?php

namespace app\controllers;

use maerduq\usm\components\UsmController;

class TestController extends UsmController {

    ...

    public function actionIndex() {
        ....
    }

    public function menuItems() {
        return [
            ['icon' => 'person', 'label' => 'Test', 'url' => ['/test/index']]
        ];
    }
```

The test controller provides one admin menu item.

_modules/admin/Module.php_

```php
<?php

namespace app\modules\admin;

use maerduq\usm\plugins\UsmPluginModule;

class AdminModule extends UsmPluginModule {

    public $controllerNamespace = 'app\modules\admin\controllers';

    public function init() {
        parent::init();
    }

    public function getAdminMenuItems() {
        return [
            ['label' => 'Clients', 'url' => ['clients/index']],
            ['label' => 'Projects', 'url' => ['procts/index']],
        ];
    }

}
```

The admin module file configures two admin menu items.

### sitemaps

Array of controllers containing a `sitemap()` method, returning an array of sitemap entries. The array item 'url' is required and should contain the input for the Url::to() function.

#### Example

In the following example, the controller _clients_ is configured as partial sitemap.

_config/web.php_

```php
...
'modules' => [
  'usm' => [
    'class' => 'maerduq\usm\UsmModule',
    ...
    'sitemaps' => [
      '/clients',
    ],
  ],
],
```

The _sitemap_ method in the controller _clients_ provides a list of all the client detail pages that are accessible through the _view_ action.

_controllers/ClientsController.php_

```php
<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Client;

class ClientsController extends Controller {

    ...

    public function actionView($clientAlias) {
        $client = Client::find()
            ->where('alias = :id', ['id' => $clientAlias])
            ->one();

        return $this->render('view', [
            'client' => $client,
        ]);
    }

    public function sitemap() {
        $clients = Client::find()->all();

        $return = [];
        foreach ($clients as $client) {
            $return[] = [
                'label' => 'Client ' . $client->name,
                'url' => ['/clients/view', 'clientAlias' => $client->alias]
            ];
        }
        return $return;
    }
}

```
