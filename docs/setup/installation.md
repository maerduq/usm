# Installation

## Starting from scratch

With the [USM Starter project](https://bitbucket.org/maerduq/usm-starter) you can quickly set up a new USM project using the composer create-project function. Just run the following command:

```bash
$ composer create-project --prefer-dist maerduq/usm-starter my-app
```

## With an existing Yii2 project

1. Make sure you have configured a database in your Yii2 project.
2. `$ composer require maerduq/usm` - to add USM to your project.
3. Add USM to your `config/web.php`

```php
...
'aliases' => [
  ...
  '@usm' => '@vendor/maerduq/usm/src',
],
...
'bootstrap' => ['usm', ...],
...
'modules' => [
  'usm' => [
    'class' => 'maerduq\usm\UsmModule',
    /* module configuration here */
  ],
],
'components' => [
  'urlManager' => [
    'rules' => [
      /* other custom rules you have */
      ['class' => 'maerduq\usm\components\RedirectRule'],
    ],
  ],
  'formatter' => [
    'class' => 'maerduq\usm\components\Formatter',
  ],
  /* When using USM style access */
  // 'user' => [
  //     'identityClass' => 'maerduq\usm\models\User',
  //     'loginUrl' => ['/usm/global/login'],
  // ],
],
...
```

4. Add USM to your `config/console.php` to setup USM migrations.

> Since version 1.6.0, you should also add USM as a module to your `config/console.php`.

```php
...
'aliases' => [
  ...
  '@usm' => '@vendor/maerduq/usm/src',
],
...
'bootstrap' => ['usm', ...],
...
'modules' => [
  'usm' => [
    'class' => 'maerduq\usm\UsmModule',
  ],
],
'controllerMap' => [
    'migrate-usm' => [
        'class' => 'yii\console\controllers\MigrateController',
        'migrationPath' => ['@vendor/maerduq/usm/src/migrations'],
        'migrationTable' => 'migration_usm',
    ]
],
...
```

5. Execute `$ php yii migrate-usm --interactive=0` to execute USM database migrations.

## Migrating from manual Yii2 module setup

Before USM was a composer package, you could install USM by installing it manually in your Yii modules folder from [this repository](https://bitbucket.org/maerduq/usm-yii2). That repository will be discontinued, the latest version is `v1.1.0`. Migration from that repository to this one is very easy, since the versioning continues. Switching this repository is therefor as easy as 1, 2, 3:

1. Delete the `/modules/usm` folder from your project.
2. Import USM using composer by executing the command:

   `$ composer require maerduq/usm:~1.1.0`

3. Search and replace every occurrence of `app\modules\usm\` in your code to `maerduq\usm\` to update the namespaces.

And done.
