# Available plugins

## [Albums](https://bitbucket.org/maerduq/usm-albums)

Plugin to upload and display pictures through albums.

### Release notes

#### v1.0.0

- Implement USM plugin by module
