# Introduction to plugins

You can write your own plugins for USM!

## Plugin registration

### 1. Controller as plugin

1. Put controller ID in the USM plugin list
2. Let your controller extend [`\maerduq\usm\components\UsmController`](https://bitbucket.org/maerduq/usm/src/master/src/components/UsmController.php)

### 2. Module as plugin (since v1.4.0)

1. Put module ID in the USM plugin list, like `albums`
2. Let your module extend [`\maerduq\usm\plugins\UsmPluginModule`](https://bitbucket.org/maerduq/usm/src/master/src/plugins/UsmPluginModule.php)
3. give the module class some functions
   1. getPluginPages() with a list of objects referencing [Plugin pages](#plugin-pages) that can be included in the menu.
   2. getAdminMenuItems() with menu items, url should start at controller level.
   3. getSitemapControllers() returning a list of controllers with a sitemap() function.

## Admin controller

The admin controllers of your USM plugin should extend the class [`\maerduq\usm\components\UsmController`](https://bitbucket.org/maerduq/usm/src/master/src/components/UsmController.php), even when you have a _Module as plugin_.

#### Behaviors

> **Do not overwrite `behaviors()` without thinking! This will throw out the access filter!**

_Example to add verbs filter to behaviors_

```php
/**
 * {@inheritdoc}
 */
public function behaviors() {
    $behaviors = parent::behaviors();
    $behaviors['verbs'] = [
        'class' => VerbFilter::className(),
        'actions' => [
            'delete'  => ['post']
        ],
    ];
    return $behaviors;
}
```

#### Menu items

public function **menuItems()** - should return an array of valid menu items:

|         |                                                                                                   |
| ------- | ------------------------------------------------------------------------------------------------- |
| `label` | The text of the menu item.                                                                        |
| `icon`  | Optional. Name of a [Bootstrap icon](https://icons.getbootstrap.com/#icons).                      |
| `url`   | Yii-style URL of the page. An URL containing only an action ID will be relative to the container. |

### Admin panel view options

You can render a intro text on your custom admin page by using the following block in the admin page view:

```php
<?php $this->beginBlock('intro'); ?>
This will be the intro text
<?php $this->endBlock(); ?>
```

## Plugin pages

Plugin pages are visitor facing pages of your plugin. When registered, they will be presented in the Plugin pages dropdown when editing a menu item and choosing for _destination type_ **Plugin page**.

You can only register Plugin pages in a _Module as plugin_.

Below is an example of the definition of Plugin pages:

```php
public function getPluginPages() {
    return [
        ['name' => 'Plugin page example', 'actionId' => 'default/index'],
        ['name' => 'Contact form example', 'actionId' => 'default/contact'],
    ];
}
```

> **Beware!** When adding a plugin page to the menu, the access level of the menu item is only used for the visibility of the menu item. You should still check the access level in the code of the plugin page yourself as well!
