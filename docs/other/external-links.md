# External links

The code of USM is hosted on Bitbucket: [https://bitbucket.org/maerduq/usm/](https://bitbucket.org/maerduq/usm/).

USM can be found on Packagist: [https://packagist.org/packages/maerduq/usm](https://packagist.org/packages/maerduq/usm).
