# Acknowledgements

### [PHP](https://www.php.net/)

Where would the internet be without PHP? Disliked by many, but indisputable a backbone of the internet.

### [Yii](https://www.yiiframework.com/)

It is not easy to pronounce, so people never understand what you are talking about, unless the know Yii themselves. It is my go-to framework to build websites with. Especially with version 2 it is a nice and feature rich framework without a lot of bloat needed to get your website working.

### [Jodit](https://xdsoft.net/jodit/)

It is hard to find a WYSIWYG (what you see is what you get) editor for the web, that is free to use, easy to use, and has advanced features like image upload and HTML editing with code highlighting. But Jodit checks all boxes.

### [Dragula](https://github.com/bevacqua/dragula)

Drag-and-drop. Such a simple user interaction, but so hard to get working perfectly. Especially if you need mobile support. Dragular is your man!

### [Bootstrap](https://getbootstrap.com/)

What would the internet look like without Bootstrap? Nobody knows. I would not call it original anymore, which is always hard for a tool like this that exists for more than 10 years. But if it has to work without the need for being shiny, Bootstrap is there for you.

### [Notify.js](https://notifyjs.jpillora.com/)

Small and to the point, just getting your popup notifications where you want them to be.
