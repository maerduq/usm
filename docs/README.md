# USM Developer Manual

> Are you an end user of USM, looking for documentation on the admin panel? Then you probably want to visit the [USM User Manual](https://docs.depaul.nl/usm/).

## Table of contents

- [📣 Release notes](release-notes.md)
- [Contributing](CONTRIBUTING.md)

### Setup

- [Installation](setup/installation.md)
- [Upgrade guide](setup/upgrade-guide.md)
- [Configuration options](setup/configuration-options.md)

### Features

- [Helpers](features/helpers.md)
- [Textblocks](features/textblocks.md)
- [USM File in your Model](features/files.md)
- [WYSIWYG editing](features/wysiwyg-editing.md)

### Plugins

- [Introduction to plugins](plugins/introduction.md)
- [Available plugins](plugins/albums.md)

### Other

- [External links](other/external-links.md)
- [Acknowledgements](other/acknowledgements.md)
