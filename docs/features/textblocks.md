# Textblocks

> With USM v2.0, a new way of integrating Textblocks in your web pages has been introduced. It now uses a widget, instead of a static function in the Textblock modal. This is correcter.
>
> As added feature, WYSIWYG editing of textblocks has been introduced!

With text blocks you can introduce parts of text in your code that is editable through the USM admin panel.

## Textblock widget

Use the Textblock widget to designate parts of web pages to be editable by your user.

### Configuration options

- **name** - the unique name of the textblock.
- **params** - a key-value array of dynamic content of your textblock. Parameters should be labeled with double brackets in your textblock text, like `{{foo}}`.
- **defaultContent** - the text of your textblock if no text has been defined in the admin panel. This is often true when the textblock is first loaded.
- **asHtml** - (default: true) whether the content should be rendered as html or not.

### Use as widget

```php
<?= Textblock::widget([
  'name' => 'home',
  'defaultContent' => '<p>Welcome {{user}}! Happy to see you.</p>',
  'params' => [
    'user' => 'me',
  ],
]) ?>
```

### Use as widget block

```php
<?php Textblock::begin(['name' => 'home',
  'params' => [
    'user' => 'me',
  ],
]) ?>
<p>
  Welcome {{user}}! Happy to see you visiting my website!
</p>
<?php Textblock::end() ?>
```

### Migrating from v1.0

- Go through your code and look for all appearances of **Textblock::read**
- Replace all Textblock::read's with Textblock::widgets, as shown in the example below:

_Before_

```php
\maerduq\usm\models\Textblock::read('home', ['user' => 'me']);
```

_After_

```php
\maerduq\usm\widgets\Textblock::widget([
  'name' => 'home',
  'params' => [
    'user' => 'me'
  ],
]);
```

## USM v1 method

```php
<?= \maerduq\usm\models\Textblock::read({label}, {replaces}) ?>
```

- `{label}` can also be an array of names of text blocks. This can be used if multiple text blocks are used in one page, to avoid multiple queries. it will return an array indexed on text block name.
- `{replaces}` is an optional array to use to replace some tags in the text block, marked by the formatting `{{...}}`.
  - Example: `['foo' => 'bar']` will replace the tag `{{foo}}` in your text block with the value `bar`.
