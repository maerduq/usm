# WYSIWYG editing

## Edit mode

Upon initialisation, USM will check for the GET variable `usm[editMode]`. If this variable is present and true, the USM edit mode will be enabled.

In USM edit mode, pages and textareas will become editable, and the toolbar will render Save and Cancel buttons. Of course edit mode will only enable when the current user is administrator.

You can fetch the current state of the USM edit mode with the following method:

```php
UsmModule::getInstance()->getEditMode();
```

## WysiwygTextarea widget

This widget wraps _Html::textArea()_ to be able to edit the contents as if it was a Word document.

```php
WysiwygTextarea::widget([
  'name' => '{inputName}',
  'value' => '{inputValue}',
  'options' => '{additionalOptions}'
])
```

### Use as ActiveField widget

Since version v2.1.0, you can also use WysiwygTextarea as a widget in your form:

```php
echo $form->field($model, 'foo')->widget(WysiwygTextarea::class, $additionalOptions);
```

## Custom WYSIWYG editor properties

The WYSIWYG editor that USM uses is Jodit. Sometimes your website needs specific configurations to work perfectly with your design. For this, you can add _wysiwygCustomConfig_ to the _Usm::renderToolbar()_ function you call in your layout.

In the following example, the Jodit toolbar needs a sticky offset of 52 pixels, because the menu bar is sticky as well:

```php
<?= Usm::renderToolbar($this, [
  'wysiwygCustomConfig' => [
    'toolbarStickyOffset' => 52,
  ],
]) ?>
```

Find more options at [https://xdsoft.net/jodit/play.html](https://xdsoft.net/jodit/play.html)
