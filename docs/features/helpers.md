# Helpers

## Formatter

USM has an extended Formatter you can use in your project. It has the following extra formatting functions:

- **asTimeago** - Display time as relative timestamp.

_config/web.php_

```php
...
'components' => [
  'formatter' => [
    'class' => 'maerduq\usm\components\Formatter',
  ],
  ...
],
...
```

## USM in your layout

How to use the `maerduq\usm\components\Usm` object in your layout files.

### Usm::getMenu()

The menu items object for displaying in your layout.

```php
<?php
NavBar::begin();
echo Nav::widget([
    'items' => Usm::getMenu()
]);
NavBar::end();
?>
```

### Usm::getBreadcrumbs()

To get the breadcrumbs for the current page.

#### Set breadcrumb for custom page

Set `$this->params['breadcrumbs']` in a view to hook into this. Set it to `false` to disable breadcrumb on that view.

#### Get breadcrumb for specific view

Use `Usm::getBreadcrumbs($url, [$lastWithLink = false])` to get the breadcrumbs for a specific page. Useful to build breadcrumbs for detail pages, of which the overview page is in the menu.

### Usm::renderToolbar(View $view)

Use `Usm::renderToolbar($this)` in your layout view to add the USM Toolbar to all pages.

## USM in your controllers

How to use the `maerduq\usm\components\Usm` object in your controller files.

### Usm::isUserAdmin()

Returns boolean whether the current user is an administrator.

```php
public function behaviors() {
  return [
    'access' => [
      'class' => AccessControl::className(),
      'rules' => [
        [
          'actions' => ['adminpage'],
          'allow' => Usm::isUserAdmin(),
        ],
        ...
      ],
    ],
  ];
}
```

### Usm::evalContent(string $text)

Use `Usm::evalContent($text)` to parse all CMS page hooks in a static text.

#### CMS page hooks

- `{{baseUrl}}` - will point to the web root folder
