# USM File in your Model

So you have a model that should contain a file, but you want to have USM handle all the hassle of managing the file itself. Sure! This is how you go about it:

- In the database model of your Model, add `file_id` column, type `$this->integer()`, with foreign key to `usm_files.id`
  - Options for onDelete and onUpdate are: RESTRICT, CASCADE, NO ACTION, SET DEFAULT, SET NULL
  - Make sure to set `onDelete = CASCADE` in order to have a easy file removal

Snippet for in your migration

```php
$this->addForeignKey('fk_example_file_id', 'example', 'file_id', 'usm_files', 'id', 'SET NULL', 'CASCADE');
```

- Use Gii to have the model class property set up
- Now, in the corresponding model,
  - Use the `\maerduq\usm\traits\ModelWithUsmFile` trait
  - Init the UsmFile in your model's `init()` function with the following parameters
    - `$fileNameAttribute` - the model's attribute used to name the USM File, default `name`
    - `$fileIdAttribute` - the model's attribute to store the USM File's ID, default `file_id`
    - `$fileAttribute` - the model's attribute to reference the USM File, default `file`
    - `$category` - the category under which to store the USM File (optional)
  - (Optional) Now you can use for instance `$this->hasUsmFile()` somewhere in your model to check wether a USM file is present

```php
class Example extends \yii\db\ActiveRecord {
    use \maerduq\usm\traits\ModelWithUsmFile;

    public function init() {
        $this->initUsmFile('name', 'file_id', 'file', 'example');
        return parent::init();
    }

    /* Example */
    public function getHasFile() {
        return ($this->hasUsmFile()) ? 'Yes' : '';
    }

    ...
}
```

- Add a file uploading field to your model form

```php
<?= $form->field($model, 'uploadedUsmFile')
    ->fileInput()
    ->hint("File size limit: {$model->getFileSizeLimit()}. " . ((!$model->isNewRecord && $model->hasUsmFile())
        ? ('You can upload another file, or ' . Html::a('delete the current file', [
            '/usm/files/delete',
            'id' => $model->file_id,
            'returnUrl' => Url::current()
        ], ['data' => ['confirm' => 'Are you sure you want to delete this file? ','method' => 'post']])) . '.'
        : ''
    )) ?>
```

- In your model's view page, use for instance `file.downloadLink:html` in your GridView columns to link to the file directly.
