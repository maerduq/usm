# Contributing

## Prerequisites

- [Docker](https://www.docker.com/)

## Getting started

With this repository, you will be able to develop on USM in a "headless" way, so there is no Yii application in this repository that integrates USM. There is a [USM Starter](https://bitbucket.org/maerduq/usm-starter) project that allows you to quickly get a USM based project up and running. To be able to easily develop on USM using this setup, execute the following steps after you created a new USM Starter project:

- `$ rm -r vendor/maerduq` - to remove the `usm` vendor directory. Since it's already downloaded with `--prefer-dist`, it won't install the source in the next step if we didn't do this.
- `$ composer require maerduq/usm:dev-development --prefer-source` - to use the development branch of USM and include it as git repository
- Now you will find a git repository in `vendor/maerduq/usm`.

If you want to develop USM and check your work through tests, you can continue this getting started.

- `$ docker compose up -d` - to start your docker containers
- `$ docker exec usm-php-1 composer install` - to install dependencies

### If you want to 



## Before proposing a pull request

Run the following command to check all pre-requisites:

```
$ docker exec usm-php-1 bash script-pre-commit
```

This script will:
- Compile all sass files in the compressed style
- Make sure all [tests](#testing) succeeded
- Whether the USM message translations are complete

## Commands during development

```
$ docker exec usm-php-1 bash script-watch
```

Will watch for SASS changes and re-compile if required.

## Testing

> All commands should be run in the `usm-php-1` docker container you set up during the [getting started](#getting-started). You can also prefix the command with `docker exec usm-php-1` to get the same result.

### Setup

- `# php tests/bin/yii migrate --interactive=0` - to setup structure for test database
- `# php vendor/bin/codecept run` - to run all tests

### Additional commands

- `# php vendor/bin/codecept run` - to run all tests
- `# php vendor/bin/codecept run --steps` - to run all tests with showing every individual step
- `# php vendor/bin/codecept run unit` - to run only unit tests
- `# php vendor/bin/codecept run functional` - to run only functional tests
- `# php vendor/bin/codecept run unit widgets/AlertTest` - to run a specific test file
- `# php vendor/bin/codecept run unit widgets/AlertTest:testSingleErrorMessage` - to run a single test
- `# php vendor/bin/codecept run --debug` - to run with debug info
- `# php tests/bin/yii fixture Customer` - to load CustomerFixture data
- `# php tests/bin/yii fixture/unload "*"` - to unload all data
