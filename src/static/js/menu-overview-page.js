var sortState = {
    active: false,
    dragula: null,
    dirty: false
};

$(function () {
    $('#sort-mode-toggle').change(function () {
        var isChecked = $(this).prop('checked');
        toggleSort();
    })
});

$(window).on('beforeunload', function () {
    if (sortState.active && sortState.dirty) {
        return 'You have unsaved changes, are you sure you want to quit?';
    }
});

function toggleSort() {
    if (sortState.active) {
        $("#create-button").removeAttr('disabled').removeClass('disabled');
        $("#menu-overview-page").removeClass('sort-mode');
        $(".item-list .title-for-sorting").hide();
        $(".item-list a").show();
        $(".item-list .label").show();

        sortState.dragula.destroy();

        if (sortState.dirty) {
            var menuItems = [];
            $("ul.item-list.root > li").each(function (index, item) {
                var el = $(item);
                var id = parseInt(el.data('id'));
                menuItems.push({
                    id: id,
                    parent_id: null,
                    position: index
                });
                el.find('ul.item-list > li').each(function (index, item) {
                    menuItems.push({
                        id: parseInt($(item).data('id')),
                        parent_id: id,
                        position: index
                    });
                });
            });

            $.ajax({
                type: 'POST',
                url: ajaxUrl + '?action=order',
                data: {
                    newOrder: menuItems
                },
                success: function () {
                    $("#sort-mode-control").notify('New order saved', 'success');
                    sortState.dirty = false;
                }
            });
        } else {
            $("#sort-mode-control").notify('No change detected, so nothing to save', 'info');
        }
    } else {
        $("#create-button").addClass('disabled').attr('disabled', 'disabled');
        $("#menu-overview-page").addClass('sort-mode');
        $(".item-list .title-for-sorting").show();
        $(".item-list a").hide();
        $(".item-list .label").hide();

        sortState.dragula = dragula([].slice.apply(document.querySelectorAll('.item-list')), {
            isContainer: function (el) {
                return el.classList.contains('item-list');
            },
            accepts: function (el, target) {
                hasSubItems = ($(el).has('ul li').length > 0);
                isRoot = $(target).hasClass('root');
                comingFromParent = ($(el).has(target).length > 0);

                if (comingFromParent) {
                    return false;
                } else if (!isRoot && hasSubItems) {
                    return false; //placing as sub items is not ok if the item itself has subitems
                }
                return true;
            },
        });

        sortState.dragula.on('drop', function (el, target) {
            sortState.dirty = true;

            droppedInRoot = $(target).hasClass('root');
            hasSubItemList = ($(el).has('ul.item-list').length > 0);
            if (!droppedInRoot && hasSubItemList) {
                $(el).find('ul.item-list').remove();
            } else if (droppedInRoot && !hasSubItemList) {
                $(el).append($("<ul class='item-list'></ul>"));
            }
        });
    }
    sortState.active = !sortState.active;
}
