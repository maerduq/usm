$(document).ready(checkType);
$("input[name='Redirect[type]']").bind('change', checkType);

var allFields = ['menu_item_id', 'pageid', 'pluginpage', 'controllerid', 'destination'];
var showFields = {
    'menu_item': ['menu_item_id'],
    'cms': ['pageid'],
    'plugin': ['pluginpage'],
    'php': ['controllerid'],
    'link': ['destination']
};

function checkType() {
    var selectedType = $('input[name="Redirect[type]"]:checked').val();
    if (showFields[selectedType] === undefined) {
        return showOnly([]);
    }
    return showOnly(showFields[selectedType]);
}


function showOnly(enabledFields) {
    allFields.forEach(function (field) {
        var el = $('.field-redirect-' + field);
        if (enabledFields.indexOf(field) > -1) {
            el.show();
        } else {
            el.hide();
        }
    });
}