$(document).ready(function () {
    $("#usm-toolbar").removeClass('loading');
    if (isToolbarMini()) {
        $("#usm-toolbar").addClass('minified');
    }
});

function isToolbarMini() {
    return (window.localStorage['usmToolbarMini'] === '1');
}

function toggleToolbarMini() {
    let isMini = isToolbarMini();

    if (isMini) {
        $("#usm-toolbar").removeClass('minified');
    } else {
        $("#usm-toolbar").addClass('minified');
    }

    window.localStorage['usmToolbarMini'] = (isMini) ? 0 : 1;
}

function saveWysiwyg(a) {
    event.preventDefault();
    wysiwygJs.isEditing = false;

    var action = $(a).attr('href');

    $form = $('<form/>', { method: "POST", action: action });
    $(".wysiwyg").each(function (index, elem) {
        var name = $(elem).attr('name');
        var value = $(elem).val();
        $form.append($('<input/>', { name: name, value: value, type: 'hidden' }));
    });

    var csrfParam = $('meta[name=csrf-param]').attr('content');
    if (csrfParam) {
        csrfToken = $('meta[name=csrf-token]').attr('content');
        $form.append($('<input/>', { name: csrfParam, value: csrfToken, type: 'hidden' }));
    }
    console.log($form);
    $form.hide().appendTo('body');
    $form.trigger('submit');
}
