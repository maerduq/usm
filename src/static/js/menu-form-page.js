$(document).ready(checkType);
$("input[name='MenuItem[type]']").bind('change', checkType);

var allFields = ['page_id', 'pluginpage', 'controllerid', 'url', 'access'];
var showFields = {
    'empty': ['access'],
    'cms': ['page_id', 'access'],
    'plugin': ['pluginpage', 'access'],
    'php': ['controllerid', 'access'],
    'link': ['url', 'access']
};

function checkType() {
    var selectedType = $('input[name="MenuItem[type]"]:checked').val();
    if (showFields[selectedType] === undefined) {
        return showOnly(['access']);
    }
    return showOnly(showFields[selectedType]);
}


function showOnly(enabledFields) {
    allFields.forEach(function (field) {
        var el = $('.field-menuitem-' + field);
        if (enabledFields.indexOf(field) > -1) {
            el.show();
        } else {
            el.hide();
        }
    });
}