/* @var wysiwygSelector - the Jquery element selector to use to find all textarea's that should become wysiwyg */
/* @var wysiwygCheckBeforeUnload - whether to block unloading the page when there are uncommited edits */
/* @var wysiwygCustomConfig - whether you want to apply custom config to the wysiwyg provider */

if (typeof wysiwygSelector === 'undefined') {
    wysiwygSelector = 'textarea.wysiwyg';
}
if (typeof wysiwygCheckBeforeUnload === 'undefined') {
    wysiwygCheckBeforeUnload = true;
}
if (typeof wysiwygCustomConfig === 'undefined') {
    wysiwygCustomConfig = {};
}

$(document).ready(function () {
    wysiwygJs.init(wysiwygSelector, wysiwygCheckBeforeUnload, wysiwygCustomConfig);
});

wysiwygJs = {
    isEditing: false,
    joditDefaultConfig: {
        buttons: "undo,redo,|,copyformat,eraser,|,bold,italic,underline,strikethrough,superscript,subscript,brush,|,paragraph,font,fontsize,|,align,outdent,indent,ul,ol,|,table,link,image,video,|,source",
        cleanHTML: {
            removeEmptyElements: false,
            fillEmptyParagraph: false
        },
        uploader: {
            url: '/usm/jodit/uploader',
            headers: {
                'X-CSRF-Token': document
                    .querySelector('meta[name="csrf-token"]')
                    .getAttribute('content')
            }
        }
    },
    editors: [],
    init: function (selector, checkBeforeUnload, customConfig) {
        wysiwygJs.isEditing = true;
        $(selector).each(function () {
            config = $.extend(wysiwygJs.joditDefaultConfig, customConfig);
            var newEditor = new Jodit(this, config);
            wysiwygJs.editors.push({
                obj: newEditor,
                initialContent: newEditor.value
            });
        });

        if (checkBeforeUnload) {
            $(window).on('beforeunload', function () {
                if (!wysiwygJs.isEditing) {
                    return;
                }
                var block = false;
                wysiwygJs.editors.forEach(function (editor) {
                    if (editor.obj.value !== editor.initialContent) {
                        block = true;
                    }
                });

                if (block) {
                    return 'You have unsaved changes, are you sure you want to quit?';
                }
            });

            $("button[type='submit'], .wysiwyg-exit").click(function () {
                wysiwygJs.isEditing = false;
            });
        }

    }
}
