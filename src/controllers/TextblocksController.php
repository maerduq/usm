<?php

namespace maerduq\usm\controllers;

use maerduq\usm\components\UsmController;
use maerduq\usm\models\Textblock;
use maerduq\usm\models\TextblockSearch;
use maerduq\usm\UsmModule;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class TextblocksController extends UsmController {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'delete'  => ['POST'],
            ],
        ];
        return $behaviors;
    }

    /**
     * Lists all Textblock models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new TextblockSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Creates a new Textblock model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Textblock();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', UsmModule::t('textblocks', 'Textblock has been created.'));
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Textblock model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', UsmModule::t('textblocks', 'Textblock has been updated.'));
            return $this->refresh();
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id = null) {
        $this->findModel($id)->delete();

        Yii::$app->session->setFlash('success', UsmModule::t('textblocks', 'Textblock has been deleted.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the Textblock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Textblock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Textblock::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(UsmModule::t('error', 'The requested page does not exist.'));
    }
}
