<?php

namespace maerduq\usm\controllers;

use maerduq\usm\components\UsmController;
use maerduq\usm\components\Usm;
use maerduq\usm\models\File;
use maerduq\usm\models\FileSearch;
use maerduq\usm\UsmModule;
use yii\helpers\Html;
use Yii;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class FilesController
 *
 * @property \maerduq\usm\UsmModule $module
 */
class FilesController extends UsmController {

    public function behaviors() {
        $behaviors = parent::behaviors();
        array_unshift($behaviors['access']['rules'], [
            'actions' => ['download', 'download-v2'],
            'allow' => true
        ]);
        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'delete' => ['POST'],
            ],
        ];
        $behaviors[] = [
            'class' => 'yii\filters\HttpCache',
            'only' => ['download-v2'],
            'etagSeed' => function ($action, $params) {
                $slug = \Yii::$app->request->get('slug');
                if ($slug === null) {
                    return null;
                }
                $model = File::findOne(['slug' => $slug]);
                if ($model === null) {
                    return null;
                }
                return $model->file_hash;
            },
            'cacheControlHeader' => 'max-age=0, must-revalidate',
        ];
        return $behaviors;
    }

    /**
     * Lists all File models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new FileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single File model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new File model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new File(['scenario' => 'new']);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', UsmModule::t('files', 'File successfully added'));
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing File model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', UsmModule::t('files', 'File successfully edited'));
            $model->touch('updated_at');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    /**
     * V1 download URL, keep intact!
     */
    public function actionDownload($id = null, $view = null) {
        if ($this->module->isUserAdmin()) {
            $access = 2;
        } elseif (!Yii::$app->user->isGuest) {
            $access = 1;
        } else {
            $access = 0;
        }

        $model = null;

        $path = explode('/', $id);
        if (count($path) == 1) { //only name
            $model = File::findOne(['category' => '', 'name' => $path[0]]);
            if ($model === null) {
                $model = File::findOne($id);
            }
        } elseif (count($path) == 2) { //catergory and name
            $model = File::findOne(['category' => $path[0], 'name' => $path[1]]);
        }

        if ($model == null || $model->access > $access) {
            throw new HttpException(404, UsmModule::t('error', 'The requested file does not exist.'));
        }

        $file = File::fileDir() . $model->file_name;

        $model->touch('last_accessed_at');

        $response = Yii::$app->getResponse();
        $response->format = Response::FORMAT_RAW;

        $response->headers->set("Pragma", "public"); // required
        $response->headers->set("Expires", "0");
        $response->headers->set("Cache-Control", "max-age=172800, public, must-revalidate");
        $response->headers->set("Content-Type", $model->file_type);

        $response->headers->set("Content-Disposition", ($view !== null ? "inline" : "attachment") .
            "; filename=\"{$model->name}.{$model->file_ext}\";");
        $response->headers->set("Content-Transfer-Encoding", "binary");
        $response->headers->set("Content-Length", filesize($file));

        if (!is_resource($response->stream = fopen($file, 'r'))) {
            throw new ServerErrorHttpException(UsmModule::t('files', 'The requested file does not exist.'));
        }

        $response->send();
    }

    public function actionDownloadV2($slug, $ext, $noTouch = false, $download = false) {
        \Yii::$app->response->format = Response::FORMAT_RAW;
        if ($this->module->isUserAdmin()) {
            $access = 2;
        } elseif (!Yii::$app->user->isGuest) {
            $access = 1;
        } else {
            $access = 0;
        }

        $model = File::findOne([
            'slug' => $slug,
            'file_ext' => $ext,
        ]);

        if ($model === null || $model->access > $access) {
            throw new NotFoundHttpException(UsmModule::t('error', 'The requested file does not exist.'));
        }

        if (!(Usm::isUserAdmin() && $noTouch)) {
            $model->touch('last_accessed_at');
        }

        $filePath = $model->filePath;

        if (!file_exists($filePath)) {
            throw new NotFoundHttpException(UsmModule::t('error', 'The requested file does not exist.'));
        }

        \Yii::$app->response->sendFile($filePath, $model->fullName, [
            'mimeType' => $model->file_type,
            'inline' => ($model->isImage && !$download),
        ]);

        return;
    }

    public function actionDelete($id = null, $returnUrl = null) {
        $model = File::findOne($id);
        if ($model == null) {
            throw new HttpException(404, UsmModule::t('error', 'The requested file does not exist.'));
        }

        $deleted = false;
        try {
            $deleted = $model->delete();
        } catch (\yii\db\IntegrityException $e) {
            \Yii::$app->session->setFlash('danger', UsmModule::t('files', 'This file is still related to something else that depends on it. Therefor it cannot be deleted!'));
            return $this->redirect(['view', 'id' => $model->id]);
        }

        if ($deleted) {
            Yii::$app->session->setFlash('success', UsmModule::t('files', 'File successfully removed'));
        } else {
            Yii::$app->session->setFlash('danger', UsmModule::t('error', 'Something went wrong.') . ' ' . Html::errorSummary($model));
        }

        $this->redirect(($returnUrl === null) ? ['index'] : $returnUrl);
    }

    /**
     * Finds the File model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return File the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = File::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(UsmModule::t('error', 'The requested file does not exist.'));
    }
}
