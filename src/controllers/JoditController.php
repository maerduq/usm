<?php

namespace maerduq\usm\controllers;

use maerduq\usm\components\Usm;
use maerduq\usm\components\UsmController;
use maerduq\usm\models\File;
use maerduq\usm\UsmModule;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use yii\web\UploadedFile;

class JoditController extends UsmController {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'uploader'  => ['POST'],
            ],
        ];
        return $behaviors;
    }

    public function actionUploader() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $files = UploadedFile::getInstancesByName('files');
        if (count($files) === 0) {
            return $this->returnError(UsmModule::t('error', 'File could not be uploaded, the file cannot be larger than {0}', [File::getFileSizeLimit()]));
        }
        $fileNames = [];
        foreach ($files as $file) {
            $nameParts = explode('.', $file->name);
            array_pop($nameParts);
            $name = implode('.', $nameParts);
            try {
                $usmFileObj = File::upsert($file, null, $name, 'editor');
            } catch (HttpException $e) {
                return $this->returnError($e->getMessage());
            }
            $fileNames[] = $usmFileObj->downloadUrl;
        }

        return [
            'success' => true,
            'time' => Usm::datetime(),
            'data' => [
                'baseurl' => '',
                'messages' => [],
                'files' => $fileNames,
                'isImages' => [true],
                'code' => 220,
            ],
        ];
    }

    private function returnError($msg) {
        return [
            'success' => false,
            'time' => Usm::datetime(),
            'data' => [
                'messages' => [$msg],
            ],
        ];
    }
}
