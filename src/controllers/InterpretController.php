<?php

namespace maerduq\usm\controllers;

use Yii;
use maerduq\usm\components\UsmController;
use maerduq\usm\models\Page;
use maerduq\usm\components\Usm;
use maerduq\usm\models\Textblock;
use maerduq\usm\UsmModule;
use yii\filters\VerbFilter;

/**
 * Description of InterpretController
 *
 * @author Paul Marcelis
 */
class InterpretController extends UsmController {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'save'  => ['POST'],
            ],
        ];
        array_unshift($behaviors['access']['rules'], [
            'allow' => true,
            'actions' => ['link', 'page'],
        ]);
        return $behaviors;
    }

    public function actionLink($href = null) {
        return $this->redirect($href, 302);
    }

    public function actionPage($id = null, $return = null) {
        if ($this->module->isUserAdmin()) {
            $access = 2;
        } elseif (Yii::$app->user->isGuest) {
            $access = 0;
        } else {
            $access = 1;
        }

        $model = Page::find()->where('id = :id && access <= :access', ['id' => $id, 'access' => $access])->one();
        if ($model == null) {
            throw new \yii\web\HttpException(404, UsmModule::t('error', 'Page not found'));
        }

        switch ($model->style) {
            case 'plain':
                $this->layout = $this->module->layout_plain;
                break;
            case 'empty':
                $this->layout = $this->module->layout_empty;
                break;
            default:
                $this->layout = $this->module->layout_container;
                break;
        }

        return $this->render('page', [
            'page' => $model,
            'returnUrl' => $return
        ]);
    }

    public function actionSave($returnUrl) {
        $textblocks = \Yii::$app->request->post('Textblock');
        if ($textblocks !== null) {
            Textblock::write($textblocks);
        }

        $pages = \Yii::$app->request->post('Page', []);
        foreach ($pages as $pageId => $list) {
            foreach ($list as $lang => $content) {
                $model = Page::findOne($pageId);
                if ($model === null) {
                    continue;
                }

                if ($lang === Usm::getBaseLanguage()) {
                    $model->content = $content;
                    $model->save();
                } else {
                    $model->saveTranslation('content', $lang, $content);
                }
            }
        }

        return $this->redirect($returnUrl);
    }
}
