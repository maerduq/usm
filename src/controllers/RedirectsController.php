<?php

namespace maerduq\usm\controllers;

use maerduq\usm\components\UsmController;
use maerduq\usm\models\MenuItem;
use maerduq\usm\models\Redirect;
use maerduq\usm\models\Page;
use maerduq\usm\models\RedirectSearch;
use maerduq\usm\UsmModule;
use Yii;
use yii\filters\VerbFilter;

class RedirectsController extends UsmController {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'delete'  => ['post']
            ],
        ];
        return $behaviors;
    }

    /**
     * Lists all Redirect models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new RedirectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionCreate() {
        $model = new Redirect();

        if (Yii::$app->request->isPost) {
            $model->attributes = $_POST['Redirect'];
            switch ($model->type) {
                case 'cms':
                    $model->destination = (isset($_POST['cms_page'])) ? $_POST['cms_page'] : null;
                    break;
                case 'menu_item':
                    $model->destination = '';
                    break;
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', UsmModule::t('urls', 'URL has been created.'));
                return $this->redirect(['update', 'id' => $model->id]);
            }
        }

        $this->setViewOptions();

        return $this->render('create', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id = null) {
        $model = Redirect::findOne($id);
        if ($model == null || $model->generated) {
            return $this->redirect(['index']);
        }

        if (Yii::$app->request->isPost) {
            $model->attributes = $_POST['Redirect'];
            switch ($model->type) {
                case 'cms':
                    $model->destination = (isset($_POST['cms_page'])) ? $_POST['cms_page'] : null;
                    break;
                case 'menu_item':
                    $model->destination = '';
                    break;
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', UsmModule::t('urls', 'URL has been edited.'));
                return $this->refresh();
            }
        }

        $this->setViewOptions();

        return $this->render('update', [
            'model' => $model
        ]);
    }

    public function actionDelete($id = null) {
        $model = Redirect::findOne($id);
        if ($model == null) {
            throw new \yii\web\HttpException(404, UsmModule::t('error', 'Page not found'));
        }

        $model->delete();
        Yii::$app->session->setFlash('success', UsmModule::t('urls', 'URL has been deleted.'));
        return $this->redirect(['index']);
    }

    public function setViewOptions() {
        $cms = Page::find()->orderBy('title')->all();
        $this->view->params['pageIdOptions'] = [];
        foreach ($cms as $item) {
            $this->view->params['pageIdOptions'][$item->id] = $item->title;
        }

        $menu = MenuItem::find()->with(['children' => function ($query) {
            return $query->orderBy(['position' => SORT_ASC]);
        }])->where(['parent_id' => null])->orderBy(['position' => SORT_ASC])->all();
        $this->view->params['menuItemIdOptions'] = [];
        $this->view->params['menuItemIdDropDownOptions'] = [];
        foreach ($menu as $item) {
            $this->view->params['menuItemIdOptions'][$item->id] = $item->title;
            if ($item->type === 'empty') {
                $this->view->params['menuItemIdDropDownOptions'][$item->id] = ['disabled' => true];
            }
            foreach ($item->children as $subItem) {
                $this->view->params['menuItemIdOptions'][$subItem->id] = '- ' . $subItem->title;
                if ($subItem->type === 'empty') {
                    $this->view->params['menuItemIdDropDownOptions'][$subItem->id] = ['disabled' => true];
                }
            }
        }
    }
}
