<?php

namespace maerduq\usm\controllers;

use maerduq\usm\components\UsmController;
use maerduq\usm\components\Usm;
use maerduq\usm\models\MenuItem;
use maerduq\usm\models\Page;
use maerduq\usm\models\Redirect;
use maerduq\usm\UsmModule;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class MenuController extends UsmController {

    public $options = [];

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'delete'  => ['post'],
                'ajax' => ['post']
            ],
        ];
        return $behaviors;
    }

    public function beforeAction($action) {
        if ($action->id === 'ajax') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionIndex($sudo = null) {
        $items = MenuItem::find()
            ->with(['redirect', 'page', 'children'])
            ->where(['parent_id' => null])
            ->orderBy(['position' => SORT_ASC])
            ->all();

        return $this->render('index', [
            'items' => $items,
            'sudo' => $sudo
        ]);
    }

    public function actionCreate($page = null, $return = null) {
        $model = new MenuItem();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', UsmModule::t('menu', 'Menu item has been created.'));
            return $this->redirect(Usm::returnUrl($return, ['update', 'id' => $model->id]));
        } elseif ($page != null) {
            $page = Page::findOne($page);
            if ($page !== null) {
                $model->visible = 1;
                $model->type = 'cms';
                $model->page_id = $page->id;
                $model->title = $page->title;
            }
        }

        $this->view->params['cms_options'] = $this->getOptions(Page::class, 'title', [], false);

        return $this->render('create', [
            'model' => $model,
            'return' => $return
        ]);
    }

    public function actionUpdate($id = null, $return = null) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', UsmModule::t('menu', 'Menu item has been edited.'));
            return $this->refresh();
        }

        $this->view->params['cms_options'] = $this->getOptions(Page::class, 'title', [], false);

        return $this->render('update', [
            'model' => $model,
            'return' => $return
        ]);
    }

    public function actionDelete($id = null) {
        $model = MenuItem::findOne($id);
        if ($model != null) {
            Redirect::deleteAll('menu_item_id = :id', ['id' => $id]);
            $model->delete();
            $sql = ($model->parent_id == null) ?
                "UPDATE " . MenuItem::tableName() . " SET position = position-1 WHERE parent_id IS NULL && position >= :order" :
                "UPDATE " . MenuItem::tableName() . " SET position = position-1 WHERE parent_id = :parent && position >= :order";
            $params = ($model->parent_id == null) ?
                ['order' => $model->position] :
                ['parent' => $model->parent_id, 'order' => $model->position];
            Yii::$app->db->createCommand($sql)->bindValues($params)->execute();
            Yii::$app->session->setFlash('success', UsmModule::t('menu', 'Menu item has been deleted.'));
        }
        $this->redirect(['index']);
        return;
    }


    public function actionAjax($action) {
        switch ($action) {
            case 'order':
                return $this->ajaxOrder();
                break;
            default:
                throw new NotFoundHttpException();
        }
    }

    public function ajaxOrder() {
        $newOrder = Yii::$app->request->post('newOrder');
        foreach ($newOrder as $op) {
            $item = MenuItem::findOne($op['id']);
            if ($item === null) {
                continue;
            }
            $item->parent_id = $op['parent_id'];
            $item->position = $op['position'];
            $item->save();
        }
        return 1;
    }

    /**
     * Finds the MenuItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MenuItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MenuItem::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(UsmModule::t('error', 'The requested page does not exist.'));
    }
}
