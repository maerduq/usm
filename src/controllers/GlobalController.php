<?php

namespace maerduq\usm\controllers;

use maerduq\usm\components\UsmController;
use maerduq\usm\components\Usm;
use maerduq\usm\models\Log;
use maerduq\usm\models\LoginForm;
use maerduq\usm\UsmModule;
use Yii;
use yii\filters\VerbFilter;

class GlobalController extends UsmController {

    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['sitemap', 'error'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => false,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout'  => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionSitemap() {
        $items = Usm::getMenu(0, true);

        foreach ($this->module->sitemaps as $sitemapAction) {
            $controller = Yii::$app->createController($sitemapAction);
            if ($controller == null) {
                continue;
            }

            $result = $controller[0]->sitemap();
            $items = array_merge($items, $result);
        }

        return $this->renderPartial('sitemap', [
            'items' => $items
        ]);
    }

    public function actionLogin($returnUrl = null) {
        // Do not process if the access_type is not of type USM
        if (!in_array(UsmModule::getInstance()->access_type, [UsmModule::ACCESS_TYPE_USM, UsmModule::ACCESS_TYPE_USM_SECURE])) {
            return $this->redirect(['default/index']);
        }

        $model = new LoginForm();

        if (\Yii::$app->request->isPost) {
            $userIP = \Yii::$app->getRequest()->getUserIP();
            $currentFailLog = Log::find()->where(['ip' => $userIP, 'action' => 'login-fail'])
                ->andWhere(['>=', 'updated_at', new \yii\db\Expression('UTC_TIMESTAMP() - (60*10)')])->one();

            if ($currentFailLog !== null && $currentFailLog->attempt >= 5) {
                $model->addError('password', UsmModule::t('error', 'You have entered an incorrect password too often. Try again in 10 minutes.'));
                return $this->render('login', [
                    'model' => $model,
                ]);
            }

            if ($model->load(\Yii::$app->request->post()) && $model->login()) {
                Log::add(Log::ACTION_LOGIN_SUCCESS);
                return $this->redirect(($returnUrl !== null) ? $returnUrl : \Yii::$app->user->returnUrl);
            } else {
                if ($currentFailLog === null) {
                    $currentFailLog = new Log();
                    $currentFailLog->ip = $userIP;
                    $currentFailLog->action = 'login-fail';
                    $currentFailLog->attempt = 1;
                    $currentFailLog->save();
                } else {
                    $currentFailLog->attempt += 1;
                    $currentFailLog->save();
                }
                $model->addError('password', UsmModule::t('error', 'Password incorrect'));
            }
        }

        return $this->render('login', [
            'model' => $model
        ]);
    }

    public function actionLogout() {
        switch ($this->module->access_type) {
            case UsmModule::ACCESS_TYPE_YII:
                \Yii::$app->user->logout();
                return $this->goHome();
            case UsmModule::ACCESS_TYPE_USM:
            case UsmModule::ACCESS_TYPE_USM_SECURE:
                \Yii::$app->user->logout();
                \Yii::$app->session->setFlash('success', UsmModule::t('usm', 'Successfully logged out'));
                return $this->redirect(['/usm']);
            default:
                throw new \yii\web\HttpException(500, UsmModule::t('error', 'USM Access rules have not been set properly'));
        }
    }
}
