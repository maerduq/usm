<?php

namespace maerduq\usm\controllers;

use maerduq\usm\components\Usm;
use maerduq\usm\components\UsmController;
use maerduq\usm\UsmModule;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class DefaultController extends UsmController {

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionKitchensink() {
        return $this->render('kitchensink');
    }

    public function actionConfiguration() {
        $usm = UsmModule::getInstance();
        $config = [
            UsmModule::t('usm', 'Yii environment') => YII_ENV,
            UsmModule::t('usm', 'Yii debug') => YII_DEBUG ? 'Enabled' : 'Disabled',
            UsmModule::t('usm', 'Application ID') => Yii::$app->id,
            UsmModule::t('usm', 'Application name') => Yii::$app->name,
            UsmModule::t('usm', 'Base language') => Usm::getBaseLanguage(),
            UsmModule::t('usm', 'All languages') => Html::ul(Usm::getLanguages()),
            UsmModule::t('usm', 'USM access type') => $usm->access_type,
            UsmModule::t('usm', 'USM enabled plugins') => $usm->plugins,
            UsmModule::t('usm', 'USM enabled sitemaps') => $usm->sitemaps,
            UsmModule::t('usm', 'USM application pages') => Html::ul(ArrayHelper::getColumn($usm->availablePagesForMenu, 'name')),
            UsmModule::t('usm', 'Standard layout') => $usm->layout_container,
            UsmModule::t('usm', 'Simple layout') => $usm->layout_plain,
            UsmModule::t('usm', 'Empty layout') => $usm->layout_empty,
        ];

        return $this->render('configuration', [
            'config' => $config
        ]);
    }
}
