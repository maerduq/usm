<?php

namespace maerduq\usm\controllers;

use maerduq\usm\components\UsmController;
use maerduq\usm\components\Usm;
use maerduq\usm\models\Page;
use maerduq\usm\models\PageSearch;
use maerduq\usm\models\Translation;
use maerduq\usm\UsmModule;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class PagesController extends UsmController {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'delete'  => ['post'],
            ],
        ];
        return $behaviors;
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate($return = null) {
        $model = new Page();
        $model->content = '<p>This is a new page!</p>';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['interpret/page', 'id' => $model->id, 'usm' => ['editMode' => 1]]);
        }

        return $this->render('create', [
            'model' => $model,
            'return' => $return,
        ]);
    }

    public function actionView($id, $returnUrl = '/usm/pages/index') {
        return $this->redirect(['interpret/page', 'id' => $id, 'returnUrl' => $returnUrl]);
    }

    public function actionUpdate($id = null, $returnUrl = null) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', UsmModule::t('pages', 'Page successfully saved'));
            return ($returnUrl === null) ? $this->refresh() : $this->redirect($returnUrl);
        }

        return $this->render('update', [
            'model' => $model,
            'returnUrl' => $returnUrl,
        ]);
    }

    public function actionDelete($id = null) {
        $model = Page::findOne($id);
        if ($model === null) {
            throw new HttpException(404, UsmModule::t('error', 'Page not found'));
        }

        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(UsmModule::t('error', 'The requested page does not exist.'));
    }
}
