<?php

namespace maerduq\usm\components;

use maerduq\usm\UsmModule;

class UsmController extends \yii\web\Controller {

    public $layout = '@usm/views/layouts/admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => UsmModule::getInstance()->isUserAdmin(),
                    ],
                ]
            ]
        ];
    }

    public function getOptions($model, $column, $where = null, $withFirst = true) {
        $options_model = new $model();

        if ($where == null) {
            $options_temp = $options_model::find()->orderBy($column . ' ASC')->all();
        } elseif (is_array($where) && !isset($where['condition'])) {
            $cond = [];
            foreach ($where as $key => $item) {
                $cond[] = "`{$key}` = :{$key}";
            }
            $cond = implode(' AND ', $cond);
            $options_temp = $options_model::find()->where($cond, $where)->orderBy($column . ' ASC')->all();
        } elseif (is_array($where) && isset($where['condition'])) {
            $options_temp = $options_model::find()->where($where['condition'], $where['params'])->orderBy($column . ' ASC')->all();
        } else {
            $options_temp = $options_model::find()->where($where)->orderBy($column . ' ASC')->all();
        }
        $options = [];
        if ($withFirst) {
            $options[null] = 'Select option';
        }
        foreach ($options_temp as $temp) {
            $options[$temp->id] = $temp->$column;
        }
        return $options;
    }

    public function menuItems() {
        return [];
    }
}
