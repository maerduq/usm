<?php

namespace maerduq\usm\components;

use Exception;
use maerduq\usm\models\User;
use maerduq\usm\UsmModule;

class UserStore extends \yii\base\BaseObject {

    const ADMIN_USERNAME = "admin";

    private static $_singleton = null;

    /**
     * @return UserStore
     */
    public static function getInstance() {
        if (static::$_singleton === null) {
            static::$_singleton = new static();
        }
        return static::$_singleton;
    }

    private function __construct($config = []) {
        parent::__construct($config);
    }

    public function getUsers() {
        $usm = UsmModule::getInstance();
        if ($usm === null) {
            throw new Exception("USM is not configured");
        } elseif (!in_array($usm->access_type, [UsmModule::ACCESS_TYPE_USM, UsmModule::ACCESS_TYPE_USM_SECURE])) {
            throw new Exception("USM access type is not set to USM");
        } elseif (empty($usm->access_password)) {
            throw new Exception("USM access password is not set");
        }

        return [
            '1' => [
                'id' => '1',
                'username' => static::ADMIN_USERNAME,
                'password' => $usm->access_password,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function findIdentity($id) {
        return isset($this->users[$id]) ? new User($this->users[$id]) : null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return User
     */
    public function findByUsername($username) {
        foreach ($this->users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new User($user);
            }
        }

        return null;
    }
}
