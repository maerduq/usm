<?php

namespace maerduq\usm\components;

use Exception;
use yii\db\ActiveRecord;
use maerduq\usm\models\Translation;

class TranslatedActiveRecord extends ActiveRecord {
    public $translations;

    public function translationSettings() {
        throw new Exception('Translation settings should be set!');
        return [
            'itemType' => '',
            'translatedAttributes' => []
        ];
    }

    /**
     * Delete translations when deleting an object
     */
    public function afterDelete() {
        $translationSettings = $this->translationSettings();
        Translation::deleteAll([
            'item_type' => $translationSettings['itemType'],
            'item_id' => $this->id,
        ]);
        return true;
    }

    public function getTranslation($attr, $lang = null, $fallback = true) {
        if ($lang === null) {
            $lang = Usm::getCurrentLanguage();
        }

        if ($lang === Usm::getBaseLanguage()) {
            return $this->$attr;
        }

        $translationSettings = $this->translationSettings();
        $result = Translation::find()->select('value')->where([
            'item_type' => $translationSettings['itemType'],
            'item_id' => $this->id,
            'lang' => $lang,
            'key' => $attr
        ])->scalar();

        if (($result === false || $result === '' || $result === null) && $fallback) {
            return $this->$attr;
        }

        return $result;
    }

    public function beforeValidate() {
        $baseLanguage = Usm::getBaseLanguage();
        $settings = $this->translationSettings();
        foreach ($settings['translatedAttributes'] as $attr) {
            if (isset($this->translations[$attr][$baseLanguage])) {
                $value = $this->translations[$attr][$baseLanguage];
                $this->$attr = $value;
            }
        }
        return true;
    }

    public function afterSave($insert, $changedAttributes) {
        $this->saveTranslations();
        return true;
    }

    /**
     * @var $input - array with first dimension the key of the field to save, seond dimension the key of the language
     */
    public function saveTranslations($translations = null) {
        if ($translations === null) {
            $translations = $this->translations;
        }

        $languages = Usm::getLanguages();
        $baseLanguage = Usm::getBaseLanguage();
        $settings = $this->translationSettings();

        foreach ($settings['translatedAttributes'] as $attr) {
            foreach ($languages as $lang) {
                if (isset($translations[$attr][$lang])) {
                    $value = $translations[$attr][$lang];

                    if ($lang === $baseLanguage) {
                        $this->$attr = $value;
                        continue;
                    }

                    $attributes = [
                        'item_type' => $settings['itemType'],
                        'item_id' => $this->id,
                        'lang' => $lang,
                        'key' => $attr
                    ];

                    $translation = Translation::find()->where($attributes)->one();
                    if ($translation === null) {
                        $translation = new Translation();
                        $translation->attributes = $attributes;
                    } elseif ($value === null || $value === '') {
                        $translation->delete();
                        continue;
                    }
                    $translation->value = $value;
                    $translation->save();
                }
            }
        }

        return true;
    }

    public function saveTranslation($attribute, $language, $value) {
        return $this->saveTranslations([$attribute => [$language => $value]]);
    }
}
