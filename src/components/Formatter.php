<?php

namespace maerduq\usm\components;

use yii\helpers\Html;

class Formatter extends \yii\i18n\Formatter {

    public function asTimeAgo($value, $config = null) {
        $sec = time() - strtotime($this->asDatetime($value, 'php:d-m-Y H:i:s'));

        if ($sec < 60) {
            return 'less than a minute ago';
        }
        $min = round($sec / 60);
        if ($min < 60) {
            return $min . ' minutes ago';
        }
        $hour = round($sec / (60 * 60));
        if ($hour < 24) {
            return $hour . ' hours ago';
        }
        $day = round($sec / (60 * 60 * 24));
        if ($day < 14) {
            return $day . ' days ago';
        }
        $week = round($sec / (60 * 60 * 24 * 7));
        return $week . ' weeks ago';
    }

    public function asUl($value, $config = null) {
        if (!is_array($value)) {
            return $value;
        }
        return Html::ul($value);
    }

    public function asHyperlink($value, $config = null) {
        if ($value === null) {
            return null;
        }
        return Html::a($value, $value, $config);
    }

    public function asTruncated($value, $length = 50) {
        $strLen = strlen($value);
        if ($strLen <= $length) {
            return $value;
        }

        $prevSpacePosition = strrpos(substr($value, 0, $length), ' ');
        $cutoff = ($prevSpacePosition === false) ? $length : $prevSpacePosition;
        $str = substr($value, 0, $cutoff) . '&hellip;';
        return $str;
    }

    public function format($value, $format) {
        if ($value === null) {
            return '';
        }
        return parent::format($value, $format);
    }
}
