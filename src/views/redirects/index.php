<?php

use yii\helpers\Html;
use yii\grid\GridView;
use maerduq\usm\UsmModule;
use maerduq\usm\widgets\IconWidget;
use yii\widgets\Pjax;

$this->params['pageHeader'] = UsmModule::t('usm', 'URLs');
?>

<?php $this->beginBlock('intro'); ?>
<p>
    <?= UsmModule::t('urls', 'With URLs you can configure custom URLs and forwarders for your website.') ?>
    <?= Html::a(UsmModule::t('urls', 'Learn more about URLs in the documentation'), 'https://docs.depaul.nl/usm/admin-panel/urls', ['target' => '_blank']) ?>.

</p>
<?php $this->endBlock(); ?>

<div class="content-toolbar">
    <?= Html::a(UsmModule::t('urls', 'Create new URL'), ['create'], ['class' => 'btn btn-primary']); ?>
</div>

<?php Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'options' => [
        'class' => 'table-responsive',
    ],
    'columns' => [
        [
            'attribute' => 'active',
            'format' => 'html',
            'filter' => $searchModel->activeOptions,
            'value' => 'activeBadge',
            'contentOptions' => [
                'style' => 'width:100px'
            ],
        ],
        [
            'attribute' => 'forward',
            'format' => 'html',
            'filter' => $searchModel->forwardOptions,
            'value' => 'forwardBadge',
            'contentOptions' => [
                'style' => 'width:100px'
            ],
        ],
        [
            'attribute' => 'url',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('/' . $model->url . ' ' . IconWidget::widget(['icon' => 'box-arrow-up-right']), '@web/' . $model->url, ['data-pjax' => 0, 'target' => '_blank']);
            }
        ],
        [
            'attribute' => 'type',
            'format' => 'raw',
            'filter' => $searchModel->typeOptions,
            'value' => 'typeLabel',
        ],
        [
            'format' => 'raw',
            'attribute' => 'generated',
            'filter' => [UsmModule::t('urls', 'Editable'), UsmModule::t('urls', 'Generated, not editable')],
            'contentOptions' => [
                'class' => 'text-right',
                'style' => 'width:160px'
            ],
            'value' => function ($model) {
                if (!$model->generated) {
                    return Html::a(IconWidget::widget(['icon' => 'pencil']), ['update', 'id' => $model->id], ['data-pjax' => 0]) . ' ' .
                        Html::a(IconWidget::widget(['icon' => 'trash']), ['delete', 'id' => $model->id], ['data-pjax' => 0, 'data-method' => 'POST', 'data-confirm' => UsmModule::t('urls', 'Are you sure to delete this URL?')]);
                } else {
                    return Html::tag('small', UsmModule::t('urls', 'Generated, not editable'), ['class' => 'not-set']);
                }
            }
        ],
    ],
]);
?>
<?php Pjax::end(); ?>