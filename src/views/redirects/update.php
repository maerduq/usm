<?php

use maerduq\usm\UsmModule;

$this->title = UsmModule::t('urls', "Edit URL: {0}", ["/" . $model->url]);
$this->params['breadcrumbs'] = [
    ['label' => UsmModule::t('usm', 'URLs'), 'url' => ['index']],
    $this->title,
];
?>


<?= $this->render('_form', ['model' => $model]) ?>