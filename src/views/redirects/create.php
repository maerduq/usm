<?php

use maerduq\usm\UsmModule;

$this->title = UsmModule::t('urls', "Create new URL");
$this->params['breadcrumbs'] = [
    ['label' => UsmModule::t('usm', 'URLs'), 'url' => ['index']],
    $this->title,
];
?>

<?= $this->render('_form', ['model' => $model]) ?>
