<?php

use maerduq\usm\assets\pages\RedirectsFormAsset;
use yii\bootstrap5\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use maerduq\usm\UsmModule;

RedirectsFormAsset::register($this);

$form = ActiveForm::begin();
?>
<?= $form->field($model, 'url', ['template' => '
   {label}
       <div class="input-group">
          <span class="input-group-text">' . Url::base(true) . '/</span>
          {input}
       </div>
       {error}
    {hint}'])->hint(UsmModule::t('urls', 'URL should be in lowercase and without special characters.')) ?>

<hr />

<?= $form->field($model, 'active')->checkbox([
    'template' => "<div class='checkbox-toggle-container'>\n{beginLabel}\n
        <div class=\"checkbox-toggle\">{input}<span class=\"slider round\"></span></div>\n
        {labelTitle}\n{endLabel}\n{error}\n{hint}\n</div>",
])->hint(UsmModule::t('urls', 'Whether the URL should be active.')) ?>

<?= $form->field($model, 'forward')->radioList($model->forwardOptions)->hint(UsmModule::t('urls', 'Whether the URL should be an endpoint or whether it should forward to the destination. (If the destination is a link, the URL will always forward).')) ?>

<hr />

<?= $form->field($model, 'type')->radioList($model->typeOptions, ['size' => 4])->hint(UsmModule::t('urls', 'What item should be linked to this URL?')) ?>


<?= $form->field($model, 'menu_item_id')->dropDownList($this->params['menuItemIdOptions'], ['prompt' => UsmModule::t('urls', 'Select a menu item...'), 'options' => $this->params['menuItemIdDropDownOptions']]) ?>

<?= $form->field($model, 'pageId')->dropDownList($this->params['pageIdOptions'], ['prompt' => UsmModule::t('urls', 'Select a USM page...')]) ?>

<?= $form->field($model, 'pluginPage')->dropDownList($model->getPluginPageOptions(), ['prompt' => UsmModule::t('urls', 'Select a Plugin page...')]) ?>

<?= $form->field($model, 'controllerId')->hint(UsmModule::t('urls', 'Do it like Yii routing does, i.e.: {0}', ['<code>/model/update?id=1</code>'])); ?>

<?= $form->field($model, 'destination')->hint(UsmModule::t('urls', 'For <b>links</b>: just copy the url with https://. For <b>controllers</b>, do it like Yii. Example: {0}', ['/model/update?id=1'])) ?>

<hr />
<div class="btn-toolbar">
    <?= Html::submitButton((($model->isNewRecord) ? UsmModule::t('urls', 'Create new URL') : UsmModule::t('urls', 'Update URL')), ['class' => 'btn btn-primary me-2']) ?>
    <?= Html::a(UsmModule::t('usm', 'Back'), ['index'], ['class' => 'btn btn-link']) ?>
</div>

<?php ActiveForm::end();
