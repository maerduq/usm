<?php

use yii\bootstrap5\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use maerduq\usm\components\Usm;
use maerduq\usm\UsmModule;

if (!isset($formConfig)) {
    $formConfig = [];
}

$form = ActiveForm::begin($formConfig);

if (!isset($withButtons)) {
    $withButtons = true;
}

$defaultReturnUrl = ['index'];
if (!isset($returnUrl)) {
    $returnUrl = $defaultReturnUrl;
}
?>

<?php foreach (Usm::getLanguages() as $lang) : ?>
    <?php if ($lang === Usm::getBaseLanguage()) : ?>
        <?= $form->field($model, 'title', [
            'template' => (count(Usm::getLanguages()) === 1) ? "{label}\n{input}\n{hint}\n{error}" :
                "{label}\n<div class='input-group'>
                <span class='input-group-text'>{$lang}</span>
                {input}
                </div>\n{hint}\n{error}"
        ]) ?>
    <?php else : ?>
        <div class="mb-3">
            <div class="input-group">
                <span class="input-group-text"><?= $lang ?></span>
                <input type="text" class="form-control" name="Page[translations][title][<?= $lang ?>]" value="<?= $model->getTranslation('title', $lang, false) ?>" />
            </div>
        </div>
    <?php endif ?>
<?php endforeach ?>

<?= $form->field($model, 'access')->radioList(Usm::getAccessOptions(), ['size' => 3])->hint(UsmModule::t('pages', 'Which users should be able to visit this page?')); ?>

<hr />

<?= $form->field($model, 'style')->dropDownList($model->styleOptions) ?>
<?= $form->field($model, 'rawEdit')->checkbox() ?>

<hr />
<div class="btn-toolbar">
    <?= Html::submitButton(UsmModule::t('usm', 'Save'), ['class' => 'btn btn-primary me-2']) ?>
    <?php if (!$model->isNewRecord) : ?>
        <?= Html::a(UsmModule::t('usm', 'Edit content'), ['interpret/page', 'id' => $model->id, 'usm' => ['editMode' => 1], 'returnUrl' => Url::current()], ['class' => 'btn btn-secondary me-2']) ?>
    <?php endif ?>
    <?php if ($withButtons) : ?>
        <?= Html::a(UsmModule::t('usm', 'Back'), $returnUrl, ['class' => 'btn btn-link']) ?>
    <?php endif ?>
</div>


<?php ActiveForm::end() ?>