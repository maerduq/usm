<?php

use maerduq\usm\UsmModule;

$this->title = UsmModule::t('pages', "Create new page");
$this->params['breadcrumbs'] = [
    ['label' => UsmModule::t('usm', 'Pages'), 'url' => ['index']],
    $this->title,
];

echo $this->render('_form', ['model' => $model, 'return' => $return]);
