<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use maerduq\usm\components\Usm;
use maerduq\usm\UsmModule;
use maerduq\usm\widgets\IconWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

$this->title = UsmModule::t('usm', "Pages");

?>

<?php $this->beginBlock('intro'); ?>
<p>
    <?= UsmModule::t('pages', 'Here you can manage the editable pages - so called USM pages - of your website. You can edit the pages as if you are editing a Word document.') ?>
    <?= Html::a(UsmModule::t('pages', 'Learn more about USM pages in the documentation'), 'https://docs.depaul.nl/usm/admin-panel/pages', ['target' => '_blank']) ?>.

</p>
<?php $this->endBlock(); ?>

<div class="content-toolbar">
    <?= Html::a(UsmModule::t('pages', 'Create new page'), ['create'], ['class' => 'btn btn-primary']); ?>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'options' => [
        'class' => 'table-responsive',
    ],
    'columns' => [
        [
            'attribute' => 'title',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a($model->title, ['interpret/page', 'id' => $model->id, 'returnUrl' => Url::current()], ['data-pjax' => 0]);
            }
        ],
        [
            'attribute' => 'access',
            'format' => 'raw',
            'value' => 'accessBadge',
            'filter' => Usm::getAccessOptions(),
        ],
        [
            'header' => 'In menu',
            'format' => 'raw',
            'value' => function ($data) {
                if ($data->menuItems == null) {
                    return Html::tag('small', Html::a(UsmModule::t('pages', "Make menu item&hellip;"), ["menu/create", "page" => $data->id, 'return' => Usm::returnUrl()], ['data-pjax' => 0]));
                } else {
                    $r = [];
                    foreach ($data->menuItems as $m) {
                        $r[] = Html::a("<span data-toggle='tooltip' data-placement='top' title='/{$m->redirect->url}'>" . $m->title . "</span>", ["menu/update", "id" => $m->id, 'return' => Usm::returnUrl()], ['data-pjax' => 0]);
                    }
                    return "<span class='badge rounded-pill text-bg-secondary'>" . count($data->menuItems) . "&times;</span> " . implode(", ", $r);
                }
            },
        ],
        [
            'header' => 'URLs',
            'format' => 'raw',
            'value' => function ($data) {
                if ($data->redirects == null) {
                    return Html::tag('i', UsmModule::t('usm', 'None'), ['class' => 'not-set']);
                } else {
                    $r = [];
                    foreach ($data->redirects as $m) {
                        $r[] = Html::a("<span>/" . $m->url . "</span>", ["redirects/update", "id" => $m->id, 'return' => Usm::returnUrl()], ['data-pjax' => 0]);
                    }
                    return "<span class='badge rounded-pill text-bg-secondary'>" . count($data->redirects) . "&times;</span> " . implode(", ", $r);
                }
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => [
                'class' => 'text-right',
                'style' => 'width:70px'
            ]
        ]
    ]
]);
?>