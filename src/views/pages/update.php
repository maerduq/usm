<?php

use maerduq\usm\UsmModule;

$this->params['pageHeader'] = UsmModule::t('pages', "Edit page: {0}", [$model->title]);
$this->params['breadcrumbs'] = [
    ['label' => UsmModule::t('usm', 'Pages'), 'url' => ['index']],
    $model->title
];
?>

<?= $this->render('_form', ['model' => $model, 'returnUrl' => $returnUrl]) ?>