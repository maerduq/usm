<?php

use maerduq\usm\components\Usm;
use maerduq\usm\UsmModule;
use yii\bootstrap5\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
    'options' => [
        'enctype' => "multipart/form-data"
    ]
]);

$sizeLimit = $model->getFileSizeLimit();

echo $form->field($model, 'file')->fileInput()->hint((($model->isNewRecord) ? '' : UsmModule::t('files', 'Upload a new file to replace the current file.') . ' ' . Html::a(UsmModule::t('files', 'Download current file'), $model->downloadUrl . "?download=1") . "."));
echo $form->field($model, 'category')->hint(UsmModule::t('files', 'Categories can be used to group your files.'));
echo $form->field($model, 'name')->hint(UsmModule::t('files', 'For if you want a different name than the name of the uploaded file.'));
echo $form->field($model, 'access')->radioList(Usm::getAccessOptions(), ['size' => 3])->hint(UsmModule::t('files', 'Which users should be able to see this file?'));
?>

<input type="submit" value="<?= ($model->isNewRecord) ? UsmModule::t('files', 'New file') : UsmModule::t('files', 'Save file') ?>" class="btn btn-primary" />
<?= Html::a(UsmModule::t('usm', 'Back'), ['index'], ['class' => 'btn btn-link']); ?>

<?php
ActiveForm::end();
