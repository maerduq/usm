<?php

use maerduq\usm\UsmModule;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model maerduq\usm\models\File */

$this->title = $model->fullNameWithCategory;
$this->params['breadcrumbs'][] = ['label' => UsmModule::t('usm', 'Files'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="file-view">
    <p class="button-toolbar">
        <?= Html::a(UsmModule::t('usm', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(UsmModule::t('usm', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => UsmModule::t('usm', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(UsmModule::t('usm', 'Download'), $model->downloadUrl . '?download=1', ['class' => 'btn btn-secondary', 'target' => '_blank']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'accessBadge:html',
            'category',
            'fullName',
            'file_size_readable',
            'file_hash',
            'last_accessed_at:datetime',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

    <h4><?= UsmModule::t('files', 'File referencing information') ?></h4>
    <?= $model->fileReferencingInfo ?>

    <?php if ($model->isImage) : ?>
        <h4><?= UsmModule::t('files', 'This file is an image') ?></h4>
        <?= Html::a(Html::img($model->downloadUrl . "?noTouch=1&t=" . time(), ['class' => 'img-thumbnail', 'style' => 'max-width:400px']), $model->downloadUrl . '?noTouch=1', ['target' => '_blank']) ?>
    <?php endif ?>

</div>