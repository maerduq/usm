<?php

use maerduq\usm\components\Usm;
use maerduq\usm\models\File;
use maerduq\usm\UsmModule;
use maerduq\usm\widgets\IconWidget;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = UsmModule::t('usm', 'Files');
?>

<?php $this->beginBlock('intro'); ?>
<p>
    <?= UsmModule::t('files', 'After uploading a file here, you can use it in your website.') ?>
    <?= Html::a(UsmModule::t('files', 'Learn more about files in the documentation'), 'https://docs.depaul.nl/usm/admin-panel/files', ['target' => '_blank']) ?>.
</p>
<?php $this->endBlock(); ?>

<?php if (!File::isFileDirectoryWritable()) : ?>
    <div class="alert alert-danger">
        <?= UsmModule::t('error', 'The directory for USM files ({0}) is not writable! Make sure to make this directory writable to be able to upload (new) files.', [File::fileDir()]) ?>
    </div>
<?php endif ?>

<div class="content-toolbar">
    <?= Html::a(UsmModule::t('files', 'Upload new file'), ['create'], ['class' => 'btn btn-primary']) ?>
</div>

<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'table-responsive',
        ],
        'columns' => [
            [
                'attribute' => 'category',
                'filter' => ArrayHelper::index(File::getAvailableCategories(), function ($a) {
                    return $a;
                }),
                'value' => function ($model) {
                    return ($model->category === null) ? '-' : $model->category;
                }
            ],
            [
                'attribute' => 'fullName',
                'value' => function ($model) {
                    return Html::a($model->fullName, ['view', 'id' => $model->id]);
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'access',
                'filter' => Usm::getAccessOptions(),
                'value' => 'accessBadge',
                'format' => 'html',
                'contentOptions' => [
                    'style' => 'width:100px;'
                ],
            ],
            [
                'attribute' => 'file_size',
                'value' => function ($model) {
                    return $model->file_size_readable;
                },
                'filter' => false,
                'contentOptions' => [
                    'style' => 'text-align:right'
                ]
            ],
            [
                'attribute' => 'last_accessed_at',
                'format' => 'date',
                'filter' => false,
            ],
            [
                'attribute' => 'created_at',
                'format' => 'date',
                'filter' => false,
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{download} {view} {update} {delete}',
                'buttons' => [
                    'download' => function ($url, $model) {
                        return Html::a(IconWidget::widget(['icon' => 'download']), $model->downloadUrl . '?download=1', ['data-pjax' => 0]);
                    },
                ],
                'headerOptions' => [
                    'style' => 'width:6rem'
                ]
            ]
        ]
    ]);
    ?>
</div>