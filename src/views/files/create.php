<?php

use maerduq\usm\UsmModule;

$this->title = UsmModule::t('files', 'Upload new file');
$this->params['breadcrumbs'] = [
    ['label' => UsmModule::t('usm', 'Files'), 'url' => ['index']],
    $this->title,
];
?>

<?= $this->render('_form', ['model' => $model]) ?>
