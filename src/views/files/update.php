<?php

use maerduq\usm\UsmModule;

$this->title = UsmModule::t('files', 'Edit file: {0}', [$model->fullNameWithCategory]);
$this->params['breadcrumbs'][] = ['label' => UsmModule::t('usm', 'Files'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fullNameWithCategory, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = UsmModule::t('usm', 'Update');
?>

<?= $this->render('_form', ['model' => $model]) ?>