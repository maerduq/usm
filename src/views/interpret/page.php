<?php

use maerduq\usm\components\Usm;
use maerduq\usm\UsmModule;
use maerduq\usm\widgets\WysiwygTextarea;

$this->title = $page->getTranslation('title');
$this->params['pageHeader'] = $this->title;

?>

<?= $this->render('_toolbar', ['mode' => 'PAGE', 'page' => $page, 'returnUrl' => $returnUrl]) ?>

<div id="the-page-content">
    <?php if (UsmModule::getInstance()->getEditMode()) : ?>
        <?= WysiwygTextarea::widget(['name' => "Page[{$page->id}][" . \Yii::$app->language . "]", 'value' => $page->getTranslation('content')]) ?>
    <?php else : ?>
        <?= Usm::evalContent($page->getTranslation('content')); ?>
    <?php endif ?>
</div>