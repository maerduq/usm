<?php

/** @var yii\web\View $this */

use maerduq\usm\components\Usm;
use maerduq\usm\assets\ToolbarAsset;
use maerduq\usm\UsmModule;
use maerduq\usm\widgets\IconWidget;
use yii\helpers\Html;
use yii\helpers\Url;

if (isset($wysiwygCustomConfig) && $wysiwygCustomConfig !== null) {
    $this->registerJsVar('wysiwygCustomConfig', $wysiwygCustomConfig);
}
ToolbarAsset::register($this);

if (!UsmModule::getInstance()->isUserAdmin()) {
    return false;
}

if (UsmModule::getInstance()->isToolbarRendered()) {
    return false;
}

if (!isset($mode)) {
    $mode = 'DEFAULT';
}
if (!isset($return)) {
    $return = null;
}

?>
<div id="usm-toolbar" class="loading">
    <div onclick="toggleToolbarMini()" class="usm-toolbar-toggler show-minified">
        <?= IconWidget::widget(['icon' => 'caret-left', 'options' => ['class' => 'icon-close']]); ?>
        <?= IconWidget::widget(['icon' => 'caret-right', 'options' => ['class' => 'icon-open']]); ?>
    </div>
    <b class="usm-toolbar-title">
        <?= UsmModule::t('usm', 'USM Toolbar') ?>
    </b>

    <?= Html::a(UsmModule::t('usm', 'Go to Admin Panel'), ['/usm']) ?>
    <?php if (isset($returnUrl)) : ?>
        <?= Html::a(UsmModule::t('usm', 'Go back'), Usm::returnUrl($returnUrl, ['/usm'])); ?>
    <?php endif ?>

    <span class='usm-toolbar-separator'></span>

    <?php if ($mode === 'PAGE') : ?>

        <p><?= UsmModule::t('pages', 'Type of page') ?>: <b><?= UsmModule::t('usm', 'USM page') ?></b></p>
        <p><?= UsmModule::t('pages', 'Page name') ?>: <b><?= $page->title ?></b></p>

        <?= Html::a(UsmModule::t('pages', 'Edit page properties'), ['/usm/pages/update', 'id' => $page->id, 'returnUrl' => Url::current()]) ?>

        <span class='usm-toolbar-separator'></span>
    <?php endif ?>

    <?php if (!UsmModule::getInstance()->getEditMode()) : ?>
        <?= Html::a(UsmModule::t('usm', 'Start editing'), Url::current(['usm' => ['editMode' => 1]]), ['class' => 'usm-toolbar-button']) ?>
        <?php if (($getReturnUrl = \Yii::$app->request->get('returnUrl', null)) !== null) : ?>
            <?= Html::a(UsmModule::t('usm', 'Back'), $getReturnUrl) ?>
        <?php endif ?>
    <?php else : ?>
        <?= Html::a(IconWidget::widget(['icon' => 'save2']) . ' ' . UsmModule::t('usm', 'Save changes'), ['/usm/interpret/save', 'returnUrl' => Url::current(['usm' => ['editMode' => null]])], ['onclick' => 'saveWysiwyg(this)', 'class' => 'usm-toolbar-button button-save wysiwyg-exit show-minified']) ?>
        <?= Html::a(IconWidget::widget(['icon' => 'x-lg']) . ' ' . UsmModule::t('usm', 'Cancel'), \Yii::$app->request->get('returnUrl', Url::current(['usm' => ['editMode' => null]])), ['class' => 'usm-toolbar-button button-cancel show-minified']) ?>
    <?php endif ?>
</div>