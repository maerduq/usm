<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap5\ActiveForm $form */
/** @var maerduq\usm\models\LoginForm $model */

use maerduq\usm\UsmModule;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap5\ActiveForm;

$this->params['pageHeader'] = UsmModule::t('usm', 'Login');
$this->context->layout = 'admin_empty';
$this->registerCss('
body {
    background: #f8f8f8;
}
#wrapper > * {   
    padding: 0 1rem;
    max-width: 400px;
    width: 100%;
}
form {
    width: 100%;
}');
?>

<div id="wrapper" class="d-flex flex-column align-items-center justify-content-center pb-5">
    <div>
        <h2><?= \Yii::$app->name ?></h2>
        <p><?= UsmModule::t('usm', 'Please provide your password to log into the website.') ?></p>
        <?php
        foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
            if ($key == 'error') {
                $key = 'danger';
            }
            echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
        }
        ?>
    </div>

    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'password')->passwordInput(['autofocus' => '']) ?>

    <?= $form->field($model, 'rememberMe')->checkbox() ?>

    <div class="btn-toolbar">
        <?= Html::submitButton(UsmModule::t('usm', 'Login'), ['class' => 'btn btn-primary me-2']) ?>
        <?= Html::a(UsmModule::t('usm', 'Back'), Url::base(true), ['class' => 'btn btn-link']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>