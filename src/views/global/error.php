<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use maerduq\usm\UsmModule;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;
$this->params['breadcrumbs'] = false;

?>
<div class="site-error">

    <div class="alert alert-danger">
        <?php if ($exception->statusCode === 404) : ?>
            <?= UsmModule::t('error', 'The page you are trying to visit does not exist. Please check your url or return to home to try again') ?>
        <?php else : ?>
            <?= $message ?>
        <?php endif ?>
    </div>
    <p>
        <?= Html::a(UsmModule::t('usm', 'Back'), Url::home()) ?>
    </p>

</div>