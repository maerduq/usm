<?php

use maerduq\usm\UsmModule;

$this->title = UsmModule::t('textblocks', 'Create new textblock');
$this->params['breadcrumbs'] = [
    ['label' => UsmModule::t('usm', 'Textblocks'), 'url' => ['index']],
    $this->title,
];

?>

<?= $this->render('_form', ['model' => $model]) ?>
