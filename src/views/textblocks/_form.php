<?php

use maerduq\usm\components\Usm;
use maerduq\usm\UsmModule;
use maerduq\usm\widgets\WysiwygTextarea;
use yii\bootstrap5\ActiveForm;
use yii\helpers\Html;

$languages = Usm::getLanguages();
?>

<?php $form = ActiveForm::begin(); ?>

<?php if ($model->isNewRecord) : ?>
    <?= $form->field($model, 'name') ?>
<?php else : ?>
    <?= $form->field($model, 'name')->staticControl() ?>
<?php endif ?>

<?php foreach ($languages as $lang) : ?>
    <div class="mb-3">
        <label class="control-label">
            <?= $model->getAttributeLabel('text') ?>
            <?php if (count($languages) > 1) : ?>
                (<?= $lang ?>)
            <?php endif ?>
        </label>
        <?= WysiwygTextarea::widget(['name' => 'Textblock[translations][text][' . $lang . ']', 'value' => $model->getTranslation('text', $lang, false), 'options' => ['style' => 'height:200px', 'class' => 'form-control']]) ?>
    </div>
<?php endforeach ?>

<?= $form->field($model, 'description')->textarea() ?>

<?= Html::submitButton(UsmModule::t('usm', 'Save'), ['class' => 'btn btn-primary']) ?>
<?= Html::a(UsmModule::t('usm', 'Back'), ['index'], ['class' => 'btn btn-link']) ?>

<?php ActiveForm::end(); ?>