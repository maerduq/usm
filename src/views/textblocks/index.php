<?php

use maerduq\usm\UsmModule;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = UsmModule::t('usm', 'Textblocks');

?>

<?php $this->beginBlock('intro'); ?>
<p>
    <?= UsmModule::t('textblocks', 'With textblocks, specific parts of a web page can be made editable. Your developer determines where and how textblocks are used in your website.') ?>
    <?= Html::a(UsmModule::t('textblocks', 'Learn more about textblocks in the documentation'), 'https://docs.depaul.nl/usm/admin-panel/textblocks', ['target' => '_blank']) ?>.
</p>
<p>
    <?= UsmModule::t('textblocks', 'Often, you can use the edit mode in the toolbar to edit textblocks directly at the place they are used in your website.') ?>
    <?= Html::a(UsmModule::t('textblocks', 'Learn more about using the toolbar in the documentation'), 'https://docs.depaul.nl/usm/features/toolbar', ['target' => '_blank']) ?>.

</p>
<?php $this->endBlock(); ?>

<div class="content-toolbar">
    <?= Html::a(UsmModule::t('textblocks', 'Create new textblock'), ['create'], ['class' => 'btn btn-primary']); ?>
</div>

<?php Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'options' => [
        'class' => 'table-responsive',
    ],
    'columns' => [
        'name',
        [
            'attribute' => 'text',
            'value' => 'strippedText',
            'format' => ['truncated', 230],
        ],
        'description:ntext',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'contentOptions' => [
                'class' => 'text-right',
                'style' => 'width:50px',
            ]
        ]
    ]
]);
?>
<?php Pjax::end(); ?>