<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap5\Breadcrumbs;
use maerduq\usm\components\Usm;
use maerduq\usm\UsmModule;
use maerduq\usm\widgets\IconWidget;

$this->beginContent('@usm/views/layouts/admin_empty.php');

if (!isset($this->params['pageHeader']) && isset($this->title)) {
    $this->params['pageHeader'] = $this->title;
}

if (isset($this->params['pageHeader'])) {
    $this->params['pageHeader'] = strip_tags($this->params['pageHeader']);
    if (!isset($this->params['breadcrumbs'])) {
        $this->params['breadcrumbs'] = [$this->params['pageHeader']];
    }
}

?>
<div id="wrapper" class="d-flex">
    <nav id="sidebar" class="d-flex flex-column py-4 px-3">
        <div class="menu-toggle">
            <button type="button" id="sidebarCollapse" class="btn btn-primary">
                <?= IconWidget::widget(['icon' => 'list', 'options' => ['class' => 'icon-open']]); ?>
                <?= IconWidget::widget(['icon' => 'caret-left', 'options' => ['class' => 'icon-close']]); ?>
                <span class="visually-hidden"><?= UsmModule::t('usm', 'Toggle Menu') ?></span>
            </button>
        </div>
        <h1 class="mb-3">
            <a href="<?= Url::to(['/usm']) ?>" class="title"><?= UsmModule::t('usm', 'Admin Panel') ?></a>
            <a href="<?= Url::to(['/usm']) ?>" class="subtitle"><?= Yii::$app->name ?></a>
        </h1>
        <div class="components flex-grow-1 mb-3">
            <?php if (Usm::isUserAdmin()) : ?>
                <?php
                $menuItems = UsmModule::getInstance()->getAdminMenu();
                $menuItems[] = ['label' => UsmModule::t('usm', 'Open website') . ' &nbsp;' . IconWidget::widget(['icon' => 'box-arrow-up-right', 'options' => ['style' => 'margin-top:-3px']]), 'url' => Url::base(true), 'linkOptions' => ['target' => '_blank']];
                $menuItems[] = ['label' => UsmModule::t('usm', 'Log out'), 'url' => ['/usm/global/logout'], 'linkOptions' => ['data-method' => 'POST']];
                ?>
                <?= Html::ul($menuItems, [
                    'class' => 'list-unstyled',
                    'item' => function ($item) {
                        $label = $item['label'];
                        if (isset($item['icon'])) {
                            $label = IconWidget::widget(['icon' => $item['icon'], 'options' => ['class' => 'me-2']]) . $label;
                        }
                        if (!isset($item['linkOptions'])) {
                            $item['linkOptions'] = [
                                'class' => '',
                            ];
                        }
                        if (!isset($item['linkOptions']['class'])) {
                            $item['linkOptions']['class'] = '';
                        }
                        $item['linkOptions']['class'] .= ' d-block py-2';
                        return Html::tag('li', Html::a($label, $item['url'], $item['linkOptions']), ['class' => ((Usm::isMenuItemActive($item) ? 'active' : ''))]);
                    }
                ]) ?>
            <?php endif ?>
        </div>

        <div class="footer">
            <p><?= UsmModule::t('usm', 'Ultimate Site Management is a product developed by {0}.', Html::a('dePaul Programming', 'https://www.depaul.nl', ['target' => '_blank'])) ?></p>
            <p>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                <?= UsmModule::t('usm', 'This template is made by {0}.', Html::a('Colorlib.com', 'https://colorlib.com', ['target' => "_blank"])) ?>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
        </div>

    </nav>

    <!-- Page Content  -->
    <div id="main" class="flex-grow-1 pb-4">
        <?php if (isset($this->params['pageHeader']) || isset($this->params['breadcrumbs']) || isset($this->blocks['intro'])) : ?>
            <div id="top" class="py-4 bg-light">
                <div class="container">
                    <?php if (isset($this->params['pageHeader'])) : ?>
                        <h1 class="mb-1"><?= $this->params['pageHeader'] ?></h1>
                    <?php endif ?>
                    <?php if (isset($this->params['breadcrumbs'])) : ?>
                        <?= Breadcrumbs::widget([
                            'links' => $this->params['breadcrumbs'],
                            'homeLink' => ['label' => UsmModule::t('usm', 'Admin Panel'), 'url' => ['/usm']],
                            'options' => [
                                'class' => 'mb-3',
                            ],
                        ]) ?>
                    <?php endif ?>
                    <?php if (isset($this->blocks['intro'])) : ?>
                        <div class="intro">
                            <?= $this->blocks['intro'] ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="container content mt-4">
            <?php
            foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
                if ($key == 'error') {
                    $key = 'danger';
                }
                echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
            }
            ?>
            <?= $content; ?>
        </div>
    </div>
</div>
<?php
$this->endContent();
