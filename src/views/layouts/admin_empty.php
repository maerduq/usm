<?php

/**
 * This layout is used for the USM login page.
 */

use yii\helpers\Html;
use maerduq\usm\assets\AdminAsset;
use maerduq\usm\UsmModule;

$asset = AdminAsset::register($this);
$usm = UsmModule::getInstance();

if (!isset($this->title) && isset($this->params['pageHeader'])) {
    $this->title = strip_tags($this->params['pageHeader']);
}

$defaultTitle = UsmModule::t('usm', 'Admin Panel') . ' ' . Yii::$app->name;
if (isset($this->title)) {
    $this->title = $this->title . ' | ' . $defaultTitle;
} else {
    $this->title = $defaultTitle;
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="apple-touch-icon" sizes="57x57" href="<?= $asset->baseUrl ?>/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= $asset->baseUrl ?>/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= $asset->baseUrl ?>/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= $asset->baseUrl ?>/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= $asset->baseUrl ?>/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= $asset->baseUrl ?>/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= $asset->baseUrl ?>/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= $asset->baseUrl ?>/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= $asset->baseUrl ?>/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?= $asset->baseUrl ?>/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= $asset->baseUrl ?>/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= $asset->baseUrl ?>/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= $asset->baseUrl ?>/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= $asset->baseUrl ?>/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#337ab7">
    <meta name="msapplication-TileImage" content="<?= $asset->baseUrl ?>/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#337ab7">

    <?= Html::csrfMetaTags() ?>
    <title><?= $this->title ?></title>
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>
    <?= $content; ?>
    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>