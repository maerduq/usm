<?php $this->beginPage() ?>
<!doctype HTML>
<html>

<head>
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= \yii\helpers\Html::encode($this->title); ?></title>
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>
    <h2><?= isset($this->params['pageHeader']) ? $this->params['pageHeader'] : "" ?></h2>
    <?= $content ?>
    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>