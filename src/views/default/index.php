<?php

use maerduq\usm\UsmModule;
use maerduq\usm\widgets\IconWidget;
use yii\helpers\Html;
use yii\helpers\Url;

$this->params['pageHeader'] = UsmModule::t('usm', "Ultimate Site Management");
$this->params['breadcrumbs'] = [
    UsmModule::t('usm', 'Home')
];

$menuItems = UsmModule::getInstance()->getAdminMenu();
unset($menuItems[0]);

$buttons = [
    [UsmModule::t('usm', 'User Manual'), 'file-person', 'https://docs.depaul.nl/usm', true],
    [UsmModule::t('usm', 'Dev. Manual'), 'file-code', 'https://bitbucket.org/maerduq/usm/src/master/docs/', true],
];
?>

<?php $this->beginBlock('intro'); ?>
<p><?= UsmModule::t('usm', 'With this USM Admin Panel, you can manage your website.') ?></p>
<?php $this->endBlock(); ?>

<section class="home-page">
    <div class="row buttons">
        <?php foreach ($menuItems as $button) : ?>
            <div class="col-xs-6 col-sm-3 col-md-2">
                <a class="btn btn-outline-primary mb-3" href="<?= Url::to($button['url']) ?>">
                    <div>
                        <?= (isset($button['icon'])) ? IconWidget::widget(['icon' => $button['icon']]) : '' ?>
                        <?= Html::tag('span', $button['label']) ?>
                    </div>
                </a>
            </div>
        <?php endforeach ?>
    </div>

    <h2><?= UsmModule::t('usm', 'More information') ?></h2>
    <div class="row buttons">
        <?php foreach ($buttons as $button) : ?>
            <div class="col-xs-6 col-sm-3 col-md-2">
                <a class="btn btn-outline-primary mb-3" href="<?= Url::to($button[2]) ?>" <?php if (isset($button[3]) && $button[3] === true) : ?>target="_blank" <?php endif ?>>
                    <div>
                        <?= IconWidget::widget(['icon' => $button[1]]) ?>
                        <?= Html::tag('span', $button[0]) ?>
                    </div>
                </a>
            </div>
        <?php endforeach ?>
    </div>
</section>