<?php

use maerduq\usm\widgets\IconWidget;
use maerduq\usm\widgets\Textblock;
use maerduq\usm\widgets\WysiwygTextarea;
use maerduq\usm\UsmModule;

$this->title = "Kitchen sink";
$this->params['breadcrumbs'] = [
    ['label' => UsmModule::t('usm', 'Website configuration'), 'url' => ['configuration']],
    $this->title,
];

?>

<h1>Textblock widget</h1>
<?php Textblock::begin(['name' => 'kitchensink']) ?>
<p>This is a <b>textblock</b>.</p>
<p>A textblock allows you to edit a part of a webpage.</p>
<?php Textblock::end() ?>

<h1>Icon widget</h1>
<div class="d-flex align-items-center">
    <?= IconWidget::widget(['icon' => 'airplane', 'options' => ['class' => 'me-3']]) ?>
    <span class="fs-1"><?= IconWidget::widget(['icon' => 'eye']) ?></span>
</div>

<h1>WYSIWYG widget</h1>
<?= WysiwygTextarea::widget(['name' => 'foo', 'value' => 'bar']) ?>