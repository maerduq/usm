<?php

use maerduq\usm\UsmModule;
use yii\helpers\Html;

$this->title = UsmModule::t('usm', 'Website configuration');
$this->params['breadcrumbs'] = [
    $this->title,
];

?>

<?php $this->beginBlock('intro'); ?>
<p><?= UsmModule::t('usm', 'The table below gives you an overview of all technical settings of your website. This page is mainly used by the developer of this website.') ?></p>
<?php $this->endBlock(); ?>

<div class="row">
    <div class="col-md-8">
        <table class="table table-striped table-bordered site-config">
            <tbody>
                <?php foreach ($config as $key => $value) : ?>
                    <tr>
                        <th style="width:200px"><?= $key ?></th>
                        <td>
                            <?php
                            if ($value == null) {
                                echo Html::tag('i', Yii::t('app', '(not set)'), ['class' => 'not-set']);
                            } elseif (is_array($value)) {
                                echo Html::ul($value);
                            } else {
                                echo $value;
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
<p><?= Html::a(UsmModule::t('usm', 'Check how various parts of USM look with your current configuration.'), ['kitchensink']) ?>