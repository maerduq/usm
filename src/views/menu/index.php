<?php

use maerduq\usm\assets\pages\MenuOverviewAsset;
use maerduq\usm\components\Usm;
use maerduq\usm\UsmModule;
use yii\helpers\Html;
use yii\helpers\Url;

$this->params['pageHeader'] = UsmModule::t('usm', 'Menu');

$this->registerJsVar('ajaxUrl', Url::to(['ajax']));
$this->registerJsVar('accessOptions', Usm::getAccessOptions());
MenuOverviewAsset::register($this);

?>
<?php $this->beginBlock('intro'); ?>
<p>
    <?= UsmModule::t('menu', 'On this page you can edit the menu of your website.') ?>
    <?= Html::a(UsmModule::t('menu', 'Learn more about editing the menu in the documentation'), 'https://docs.depaul.nl/usm/admin-panel/menu', ['target' => '_blank']) ?>.

</p>
<?php $this->endBlock(); ?>

<section id="menu-overview-page">

    <div class="content-toolbar">
        <?= Html::a(UsmModule::t('menu', 'Create new menu item'), ['create'], ['class' => 'btn btn-primary me-2', 'id' => 'create-button']); ?>
        <label id="sort-mode-control">
            <div class="checkbox-toggle">
                <input type="checkbox" id="sort-mode-toggle">
                <span class="slider round"></span>
            </div>
            <?= UsmModule::t('menu', 'Order mode for menu items') ?>
        </label>
    </div>
    <?php if (count($items) == 0) : ?>
        <p><i><?= UsmModule::t('menu', 'You don\'t have a menu yet.') ?></i></p>
    <?php endif; ?>

    <ul class="item-list list-unstyled root">
        <?php foreach ($items as $item) : ?>
            <li data-id="<?= $item->id ?>">
                <?= $this->render('_item', ['model' => $item]) ?>
                <?php /* The following line should stay in one line to eliminate rendering whitespace within the item-list, while we use css :empty to highlight this element when ordering mode is active */ ?>
                <ul class="item-list list-unstyled"><?php foreach ($item->getChildren()->with(['redirect', 'page'])->orderBy(['position' => SORT_ASC])->each() as $subItem) : ?><?= Html::tag('li', $this->render('_item', ['model' => $subItem]), ['data-id' => $subItem->id]) ?><?php endforeach ?></ul>
            </li>
        <?php endforeach ?>
    </ul>
</section>