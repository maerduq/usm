<?php

use maerduq\usm\UsmModule;

$this->title = UsmModule::t('menu', "Edit menu item: {0}", [$model->title]);
$this->params['breadcrumbs'] = [
    ['label' => UsmModule::t('usm', 'Menu'), 'url' => ['index']],
    $this->title,
];

?>

<?= $this->render('_form', ['model' => $model, 'return' => $return]);
