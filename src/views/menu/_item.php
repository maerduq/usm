<?php

use yii\helpers\Html;
use yii\helpers\Url;
use maerduq\usm\components\Usm;
use maerduq\usm\UsmModule;
use maerduq\usm\widgets\IconWidget;

?>
<div class="media <?php if ($model->visible != 1) : ?>bg-light<?php endif ?> d-flex align-items-center py-2 px-3">
    <div class="name flex-grow-1 fs-5">
        <?php
        if ($model->redirect !== null) {
            echo Html::tag('span', Html::a($model->title, ['/' . $model->redirect->url, 'returnUrl' => Url::current()]), ['data' => ['toggle' => "tooltip", 'placement' => "right"], 'title' => '/' . $model->redirect->url]);
            echo Html::tag('span', $model->title, ['class' => 'title-for-sorting', 'style' => 'display:none']);
        } else {
            echo Html::tag('span', $model->title);
        }
        ?>
    </div>
    <div class="actions">
        <?php if ($model->visible !== 1) : ?>
            <span class='badge rounded-pill text-bg-secondary lbl-visible'><?= UsmModule::t('menu', 'Hidden') ?></span>
        <?php endif ?>

        <?php
        $accessOptions = Usm::getAccessOptions();
        $accessColors = Usm::$accessColors;
        $access = $model->access;

        echo Html::tag('span', $accessOptions[$access], ['class' => 'badge rounded-pill lbl-access text-bg-' . $accessColors[$access]]);

        switch ($model->type) {
            case 'empty':
                echo "<span class='badge rounded-pill text-bg-secondary'>" . UsmModule::t('usm', 'Empty menu item') . "</span>";
                break;
            case 'cms':
                echo Html::a('<span class="badge rounded-pill text-bg-success" data-toggle="tooltip" data-placement="top" title="' . $model->page->title . '">' . UsmModule::t('usm', 'USM page') . '</span>', ['interpret/page', 'id' => $model->page->id, 'usm' => ['editMode' => 1], 'returnUrl' => Url::current()]);
                break;
            case 'plugin':
                echo '<span class="badge rounded-pill text-bg-danger" data-toggle="tooltip" data-placement="top" title="' . UsmModule::getInstance()->reverseLookupPluginPage($model->url) . '">' . UsmModule::t('usm', 'Plugin page') . '</span>';
                break;
            case 'php':
                echo '<span class="badge rounded-pill text-bg-warning" data-toggle="tooltip" data-placement="top" title="' . $model->url . '">' . UsmModule::t('usm', 'Yii controller') . '</span>';
                break;
            case 'link':
                echo Html::a('<span class="badge rounded-pill text-bg-info" data-toggle="tooltip" data-placement="top" title="' . $model->url . '">' . UsmModule::t('usm', 'Link') . '</span>', $model->url, ['target' => '_blank']);
                break;
        }
        ?>
    </div>
    <div class="buttons ms-2">
        <?php if ($model->redirect !== null) : ?>
            <?= Html::a(IconWidget::widget(['icon' => 'eye']), ['/' . $model->redirect->url, 'returnUrl' => Url::current()]) ?>
        <?php endif; ?>
        <?= Html::a(IconWidget::widget(['icon' => 'pencil']), ['update', 'id' => $model->id]) ?>
        <?= Html::a(IconWidget::widget(['icon' => 'trash']), ['delete', 'id' => $model->id], ['data' => ['method' => 'POST', 'confirm' => UsmModule::t('menu', 'Are you sure to delete this menu item?')]]) ?>
    </div>
</div>