<?php

use maerduq\usm\assets\pages\MenuFormAsset;
use yii\bootstrap5\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use maerduq\usm\components\Usm;
use maerduq\usm\UsmModule;

MenuFormAsset::register($this);

$form = ActiveForm::begin();
?>

<?= $form->field($model, 'visible')->checkbox([
    'template' => "<div class='checkbox-toggle-container'>\n{beginLabel}\n
        <div class=\"checkbox-toggle\">{input}<span class=\"slider round\"></span></div>\n
        {labelTitle}\n{endLabel}\n{error}\n{hint}\n</div>",
])->hint(UsmModule::t('menu', 'Whether this item should be shown in the menu')); ?>

<?php foreach (Usm::getLanguages() as $lang) : ?>
    <?php if ($lang === Usm::getBaseLanguage()) : ?>
        <?= $form->field($model, 'title', [
            'template' => (count(Usm::getLanguages()) === 1) ? "{label}\n{input}\n{hint}\n{error}" :
                "{label}\n<div class='input-group'>
                <span class='input-group-text'>{$lang}</span>
                {input}
                </div>\n{hint}\n{error}"
        ]) ?>
    <?php else : ?>
        <div class="mb-3">
            <div class="input-group">
                <span class="input-group-text"><?= $lang ?></span>
                <input type="text" class="form-control" name="MenuItem[translations][title][<?= $lang ?>]" value="<?= $model->getTranslation('title', $lang, false) ?>" />
            </div>
        </div>
    <?php endif ?>
<?php endforeach ?>

<?= $form->field($model, 'alias')->hint(UsmModule::t('menu', 'The alias for this menu item in the address bar. Should only contain lowercase characters and dashes. If this is left blank, an URL alias will be generated from the title.')); ?>

<hr />

<?= $form->field($model, 'type')->radioList($model->typeOptions)->hint(UsmModule::t('menu', 'What item should be linked to this menu item?')); ?>

<?= $form->field($model, 'page_id')->dropDownList($this->params['cms_options'], [
    'prompt' => '- ' . UsmModule::t('pages', 'Create new page'),
])->hint(($model->isNewRecord) ? '' : Html::a(UsmModule::t('pages', 'Edit page'), ['pages/update', 'id' => $model->page_id, 'returnUrl' => Url::current()])); ?>


<?= $form->field($model, 'pluginPage')->dropDownList($model->getPluginPageOptions(), ['prompt' => UsmModule::t('menu', 'Select a plugin page...')]) ?>

<?= $form->field($model, 'controllerId')->hint(UsmModule::t('menu', 'Do it like Yii routing does, i.e.: {0}', ['<code>/model/update?id=1</code>'])); ?>

<?= $form->field($model, 'url')->hint(UsmModule::t('menu', 'Just copy-paste your URL here, including the {0} part.', ['<code>https://</code>'])); ?>

<?= $form->field($model, 'access')->radioList(Usm::getAccessOptions(), ['size' => 3])->hint(UsmModule::t('menu', 'Which users should be able to see this menu item?')); ?>

<div class="btn-toolbar">
    <?= Html::submitButton((($model->isNewRecord) ? UsmModule::t('usm', 'Create') : UsmModule::t('usm', 'Update')), ['class' => 'btn btn-primary me-2']); ?>
    <?= Html::a(UsmModule::t('usm', 'Back'), Usm::returnUrl($return, ['index']), ['class' => 'btn btn-link']); ?>
</div>
<?php ActiveForm::end(); ?>