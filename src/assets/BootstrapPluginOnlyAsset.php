<?php

namespace maerduq\usm\assets;

class BootstrapPluginOnlyAsset extends \yii\web\AssetBundle {
    /**
     * @inheritDoc
     */
    public $sourcePath = '@bower/bootstrap';

    /**
     * @inheritDoc
     */
    public $js = [
        'dist/js/bootstrap.bundle.js',
    ];
}
