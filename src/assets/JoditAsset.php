<?php

namespace maerduq\usm\assets;

class JoditAsset extends \yii\web\AssetBundle {

    public $sourcePath = '@npm/jodit/build';
    public $js = [
        'jodit.min.js',
    ];
    public $css = [
        'jodit.min.css',
    ];
}
