<?php

namespace maerduq\usm\assets\pages;

class MenuFormAsset extends \yii\web\AssetBundle {

    public $sourcePath = '@usm/static';
    public $js = [
        'js/menu-form-page.js',
    ];
    public $depends = [
        \yii\web\JqueryAsset::class,
    ];
}
