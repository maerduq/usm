<?php

namespace maerduq\usm\assets\pages;

class RedirectsFormAsset extends \yii\web\AssetBundle {

    public $sourcePath = '@usm/static';
    public $js = [
        'js/urls-form-page.js',
    ];
    public $depends = [
        \yii\web\JqueryAsset::class,
    ];
}
