<?php

namespace maerduq\usm\assets\pages;

class MenuOverviewAsset extends \yii\web\AssetBundle {

    public $sourcePath = '@usm/static';
    public $js = [
        'js/menu-overview-page.js',
    ];
    public $depends = [
        \maerduq\usm\assets\DragulaAsset::class,
        \maerduq\usm\assets\NotifyJsAsset::class,
    ];
}
