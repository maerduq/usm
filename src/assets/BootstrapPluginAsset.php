<?php

namespace maerduq\usm\assets;

class BootstrapPluginAsset extends \yii\web\AssetBundle {
    /**
     * @inheritDoc
     */
    public $sourcePath = '@bower/bootstrap';

    /**
     * @inheritDoc
     */
    public $js = [
        'dist/js/bootstrap.bundle.js',
    ];

    /**
     * @inheritDoc
     */
    public $depends = [
        BootstrapAsset::class,
    ];
}
