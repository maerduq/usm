<?php

namespace maerduq\usm\assets;

class ToolbarAsset extends \yii\web\AssetBundle {

    public $sourcePath = '@usm/static';
    public $js = [
        'js/toolbar.js',
    ];
    public $css = [
        'css/toolbar.css',
    ];
    public $depends = [
        \yii\web\JqueryAsset::class,
    ];
}
