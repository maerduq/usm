<?php

namespace maerduq\usm\assets;

class WysiwygAsset extends \yii\web\AssetBundle {

    public $sourcePath = '@usm/static';
    public $js = [
        'js/wysiwyg.js',
    ];
    public $depends = [
        \yii\web\JqueryAsset::class,
        \maerduq\usm\assets\JoditAsset::class,
    ];
}
