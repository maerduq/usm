<?php

namespace maerduq\usm\assets;

class DragulaAsset extends \yii\web\AssetBundle {

    public $sourcePath = '@npm/dragula/dist';
    public $js = [
        'dragula.min.js',
    ];
    public $css = [
        'dragula.min.css',
    ];
}
