<?php

namespace maerduq\usm\assets;

class NotifyJsAsset extends \yii\web\AssetBundle {

    public $sourcePath = '@npm/notifyjs-browser/dist/';
    public $js = [
        'notify.js',
    ];
    public $depends = [
        \yii\web\JqueryAsset::class,
    ];
}
