<?php

namespace maerduq\usm\assets;

/**
 * General style of the admin pages
 */
class AdminAsset extends \yii\web\AssetBundle {

    /**
     * @inheritDoc
     */
    public $sourcePath = '@usm/static';

    /**
     * @inheritDoc
     */
    public $css = [
        'css/app/admin-panel.css',
    ];

    /**
     * @inheritDoc
     */
    public $js = [
        'js/admin-panel.js',
    ];

    /**
     * @inheritDoc
     */
    public $depends = [
        \yii\web\JqueryAsset::class,
        \yii\web\YiiAsset::class,
    ];
}
