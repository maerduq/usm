<?php

namespace maerduq\usm\assets;

class BootstrapAsset extends \yii\web\AssetBundle {
    /**
     * @inheritDoc
     */
    public $sourcePath = '@usm/static/css/bootstrap';

    /**
     * @inheritDoc
     */
    public $css = [
        'maerduq-bootstrap.css'
    ];
}
