<?php

namespace maerduq\usm\traits;

use maerduq\usm\models\File;
use yii\db\ActiveRecord;

trait ModelWithUsmFile {
    public $uploadedUsmFile;

    private $_usmFileNameAttribute;
    private $_usmFileIdAttribute;
    private $_usmFileAttribute;
    private $_usmFileCategory;

    public function initUsmFile($fileNameAttribute = 'name', $fileIdAttribute = 'file_id', $fileAttribute = 'file', $category = null) {
        $this->_usmFileNameAttribute = $fileNameAttribute;
        $this->_usmFileIdAttribute = $fileIdAttribute;
        $this->_usmFileAttribute = $fileAttribute;
        $this->_usmFileCategory = $category;

        $this->on(ActiveRecord::EVENT_BEFORE_VALIDATE, [$this, 'usmFileBeforeValidate']);
        $this->on(ActiveRecord::EVENT_BEFORE_INSERT, [$this, 'usmFileBeforeSave']);
        $this->on(ActiveRecord::EVENT_BEFORE_UPDATE, [$this, 'usmFileBeforeSave']);
        $this->on(ActiveRecord::EVENT_AFTER_DELETE, [$this, 'usmFileAfterDelete']);
    }

    public static function getFileSizeLimit() {
        return File::getFileSizeLimit();
    }

    public function hasUsmFile() {
        $attr = $this->_usmFileIdAttribute;
        return ($this->$attr !== null);
    }

    public function getDownloadUrl() {
        $attr = $this->_usmFileAttribute;
        if ($this->$attr === null) {
            return null;
        }
        return $this->$attr->downloadUrl;
    }


    public function usmFileBeforeValidate() {
        $this->uploadedUsmFile = \yii\web\UploadedFile::getInstance($this, 'uploadedUsmFile');
        return true;
    }

    public function usmFileBeforeSave() {
        $idAttr = $this->_usmFileIdAttribute;
        $nameAttr = $this->_usmFileNameAttribute;
        if (is_object($this->uploadedUsmFile) && get_class($this->uploadedUsmFile) == "yii\web\UploadedFile") {
            $fileInfo = \maerduq\usm\models\File::upsert($this->uploadedUsmFile, $this->$idAttr, $this->$nameAttr, $this->_usmFileCategory);
            $this->$idAttr = $fileInfo->id;
        }
        return true;
    }

    public function usmFileAfterDelete() {
        $attr = $this->_usmFileAttribute;
        if ($this->$attr !== null) {
            return $this->$attr->delete();
        } else {
            return true;
        }
    }
}
