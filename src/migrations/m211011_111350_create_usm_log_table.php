<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%usm_log}}`.
 */
class m211011_111350_create_usm_log_table extends Migration {
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('{{%usm_log}}', [
            'id' => $this->primaryKey(),
            'ip' => $this->string(),
            'action' => $this->string(),
            'info' => $this->text(),
            'attempt' => $this->integer(),
            'created_at' => $this->datetime(),
            'updated_at' => $this->datetime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('{{%usm_log}}');
    }
}
