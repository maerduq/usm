<?php

use yii\db\Migration;

/**
 * Class m200926_104933_alter_redirects_type_column
 */
class m200926_104933_alter_redirects_type_column extends Migration {
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->alterColumn('usm_redirects', 'type', $this->string(30) . ' NOT NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->alterColumn('usm_redirects', 'type', "ENUM('cms','php','link','menu_item') NOT NULL");
    }
}
