<?php

use yii\db\Migration;

/**
 * Class m200926_094058_alter_usm_menu_items_type_column
 */
class m200926_094058_alter_usm_menu_items_type_column extends Migration {
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->alterColumn('usm_menu_items', 'type', $this->string(30) . ' NOT NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->alterColumn('usm_menu_items', 'type', "ENUM('empty','cms','php','link') NOT NULL");
    }
}
