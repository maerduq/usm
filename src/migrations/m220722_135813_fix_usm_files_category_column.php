<?php

use yii\db\Migration;

/**
 * Class m220722_135813_fix_usm_files_category_column
 */
class m220722_135813_fix_usm_files_category_column extends Migration {
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->alterColumn('usm_files', 'category', $this->string(60)->null());
        $this->update('usm_files', ['category' => null], ['category' => '']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->update('usm_files', ['category' => ''], ['category' => null]);
        $this->alterColumn('usm_files', 'category', $this->string(60)->notNull());
    }
}
