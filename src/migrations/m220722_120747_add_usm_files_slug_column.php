<?php

use yii\db\Migration;

/**
 * Class m220722_120747_add_usm_files_slug_column
 */
class m220722_120747_add_usm_files_slug_column extends Migration {
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('usm_files', 'slug', $this->string()->after('id'));

        $allFileIds = $this->db->createCommand("SELECT id FROM usm_files")->queryColumn();
        foreach ($allFileIds as $id) {
            $slug = bin2hex(random_bytes(10));
            $this->update('usm_files', ['slug' => $slug], ['id' => $id]);
        }

        $this->alterColumn('usm_files', 'slug', $this->string()->notNull()->unique());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropColumn('usm_files', 'slug');
    }
}
