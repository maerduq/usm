<?php

use yii\db\Migration;

/**
 * Class m220724_093527_add_usm_files_hash_column
 */
class m220724_093527_add_usm_files_hash_column extends Migration {
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('usm_files', 'file_hash', $this->string()->after('file_ext'));

        $allFiles = $this->db->createCommand("SELECT id, file_name FROM usm_files")->queryAll();
        foreach ($allFiles as $file) {
            $filePath = \Yii::$app->basePath . "/files/" . $file['file_name'];
            $hash = (file_exists($filePath)) ? hash_file('md5', $filePath) : "0";
            $this->update('usm_files', ['file_hash' => $hash], ['id' => $file['id']]);
        }

        $this->alterColumn('usm_files', 'file_hash', $this->string()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropColumn('usm_files', 'file_hash');
    }
}
