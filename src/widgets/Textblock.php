<?php

namespace maerduq\usm\widgets;

use maerduq\usm\models\Textblock as ModelsTextblock;
use maerduq\usm\UsmModule;
use yii\base\Widget;
use yii\helpers\Html;

class Textblock extends Widget {
    public $options = [];
    public $name;
    public $defaultContent;
    public $params = [];
    public $asHtml = true;

    public function init() {
        parent::init();
        ob_start();
    }

    public function run() {
        $defaultContentFromOb = ob_get_clean();
        $defaultContent = ($this->defaultContent !== null) ? $this->defaultContent : $defaultContentFromOb;
        $content = ModelsTextblock::read($this->name, $this->params, $defaultContent);

        if (!$this->asHtml) {
            $content = strip_tags($content);
        }

        if (UsmModule::getInstance()->getEditMode()) {
            $content = WysiwygTextarea::widget(['name' => "Textblock[{$this->name}][" . \Yii::$app->language . "]", 'value' => $content]);
        }

        return $content;
    }
}
