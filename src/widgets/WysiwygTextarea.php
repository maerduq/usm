<?php

namespace maerduq\usm\widgets;

use maerduq\usm\assets\WysiwygAsset;
use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * Use WysiwygTextarea directly:
 *  WysiwygTextarea::widget(['name' => 'foo', 'value' => 'bar']);
 * or use it as part of your ActiveForm
 *  $form->field($model, 'foo')->widget(WysiwygTextarea::class) ?>
 */
class WysiwygTextarea extends InputWidget {

    public function init() {
        WysiwygAsset::register($this->view);
        parent::init();
    }

    public function run() {
        if (!isset($this->options['class'])) {
            $this->options['class'] = '';
        }
        $this->options['class'] .= " wysiwyg";

        if ($this->hasModel()) {
            return Html::activeTextarea($this->model, $this->attribute, $this->options);
        }
        return Html::textarea($this->name, $this->value, $this->options);
    }
}
