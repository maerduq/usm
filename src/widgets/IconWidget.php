<?php

namespace maerduq\usm\widgets;

use yii\bootstrap5\BootstrapIconAsset;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Widget to easily render a [Bootstrap Icon](https://icons.getbootstrap.com/)
 * 
 * @param $icon - the name of the bootstrap icon you want to show
 * @param $options - additional tag options to add to the icon
 */
class IconWidget extends Widget {
    public $icon;
    public $options = [];

    public function init() {
        BootstrapIconAsset::register($this->view);
        parent::init();
    }

    public function run() {
        $class = 'bi bi-' . $this->icon;
        if (isset($this->options['class'])) {
            $class .= ' ' . $this->options['class'];
        }
        $this->options['class'] = $class;
        return Html::tag('i', '', $this->options);
    }
}
