<?php

namespace maerduq\usm;

use Yii;
use yii\base\InvalidConfigException;

class UsmModule extends \yii\base\Module {

    const ACCESS_TYPE_USM = 'usm';
    const ACCESS_TYPE_USM_SECURE = 'usm2';
    const ACCESS_TYPE_YII = 'yii';

    const PLUGIN_PAGE_LABEL_SEPARATOR = '/';

    public $controllerNamespace = 'maerduq\usm\controllers';
    public $languages = null;
    public $access_type = self::ACCESS_TYPE_USM;
    public $access_password;
    public $access_admin_check = null;
    public $layout_container = '@usm/views/layouts/default';
    public $layout_plain = '@usm/views/layouts/default';
    public $layout_empty = '@usm/views/layouts/empty';
    public $sitemaps = [];
    public $plugins = [];
    public $availablePagesForMenu = [];
    private $_menu = [];
    private $_plugins = [];
    private $_pluginModuleIds = [];
    private $_editMode = false;

    public function init() {
        parent::init();
        Yii::setAlias('@usm', __DIR__);

        if (\Yii::$app instanceof \yii\console\Application) {
            $this->_initConsole();
        } else if (\Yii::$app instanceof \yii\web\Application) {
            $this->_initWeb();
        }

        $this->registerTranslations();


        // language things
        if (!is_array($this->languages)) {
            $this->languages = [Yii::$app->language];
        } elseif ($this->languages[0] != Yii::$app->language) {
            $this->languages = array_merge([Yii::$app->language], $this->languages);
        }

        Yii::$app->layout = $this->layout_container;

        $this->_menu = [
            [
                'icon' => 'house',
                'label' => UsmModule::t('usm', 'Home'),
                'url' => ['/usm/default/index']
            ],
            [
                'icon' => 'list-nested',
                'label' => UsmModule::t('usm', 'Menu'),
                'url' => ['/usm/menu']
            ],
            [
                'icon' => 'file-earmark-text',
                'label' => UsmModule::t('usm', 'Pages'),
                'url' => ['/usm/pages']
            ],
            [
                'icon' => 'folder2-open',
                'label' => UsmModule::t('usm', 'Files'),
                'url' => ['/usm/files']
            ],
            [
                'icon' => 'card-text',
                'label' => UsmModule::t('usm', 'Textblocks'),
                'url' => ['/usm/textblocks']
            ],
            [
                'icon' => 'hash',
                'label' => UsmModule::t('usm', 'URLs'),
                'url' => ['/usm/redirects']
            ],
            [
                'icon' => 'gear-wide-connected',
                'label' => UsmModule::t('usm', 'Website configuration'),
                'url' => ['/usm/default/configuration']
            ],
        ];

        //submodule things
        $dir = dirname(__FILE__) . "/modules";
        if (file_exists($dir)) {
            $dirs = scandir($dir);
            unset($dirs[0]);
            unset($dirs[1]);
            foreach ($dirs as $item) {
                if (is_dir($dir . "/" . $item) && file_exists($dir . "/" . $item . "/" . ucfirst($item) . "Module.php")) {
                    $this->_plugins[] = $item;
                }
            }
        }

        if ($this->_plugins != []) {
            $this->setModules($this->_plugins);
            foreach ($this->_plugins as $module) {
                $menuItems = $this->getModule($module)->menuItems();
                foreach ($menuItems as $menuItem) {
                    $this->_menu[] = $menuItem;
                }
            }
        }
        foreach ($this->_plugins as $module) {
            $this->getModule($module)->menu = $this->_menu;
        }

        //new plugin things
        if ($this->plugins != []) {
            $pluginMenu = [];
            foreach ($this->plugins as $pluginName) {
                if (strpos($pluginName, '/') === false) {
                    $module = Yii::$app->getModule($pluginName);
                    if ($module !== null) { //plugin is a module
                        $this->_pluginModuleIds[] = $pluginName;
                        $menuItems = $module->getAdminMenuItems();
                        foreach ($menuItems as $menuItem) {
                            $menuItem['url'][0] = '/' . $module->id . '/' . $menuItem['url'][0];
                            $pluginMenu[] = $menuItem;
                        }

                        foreach ($module->getSitemapControllers() as $sitemapController) {
                            $this->sitemaps[] = '/' . $module->id . '/' . $sitemapController;
                        }
                        continue;
                    }
                }

                $controller = Yii::$app->createController($pluginName);
                if ($controller == null) {
                    throw new \Exception(UsmModule::t('error', 'Error while initializing USM, could not find plugin controller {0}', $pluginName));
                }
                $controller = $controller[0];
                $menuItems = $controller->menuItems();

                foreach ($menuItems as $menuItem) {
                    if (is_array($menuItem['url']) && count($menuItem['url']) === 1 && strpos($menuItem['url'][0], '/') === false) {
                        $menuItem['url'][0] = "/{$controller->id}/{$menuItem['url'][0]}";
                        if (isset($controller->module)) {
                            $menuItem['url'][0] = "/{$controller->module->id}{$menuItem['url'][0]}";
                        }
                    }
                    $pluginMenu[] = $menuItem;
                }
            }
            $this->_menu = array_merge($this->_menu, $pluginMenu);
        }
    }

    public function _initConsole() {
        $this->controllerNamespace = 'usm\commands';
    }

    public function _initWeb() {
        $usmGetVars = \Yii::$app->request->get('usm', []);
        if (isset($usmGetVars['editMode']) && !!$usmGetVars['editMode']) {
            $this->setEditMode(true);
        }

        try {
            $r = Yii::$app->request->pathInfo;
            $r_array = explode('/', $r);

            if ($r_array[0] === 'usm') {
                $components = \Yii::$app->getComponents();
                $components['errorHandler'] = array_merge($components['errorHandler'], [
                    'errorAction' => '/usm/global/error',
                ]);
                \Yii::$app->setComponents($components);

                $handler = $this->get('errorHandler');
                \Yii::$app->set('errorHandler', $handler);
                $handler->register();
            }
        } catch (InvalidConfigException $e) {
        }
    }

    public function isUserAdmin() {
        if ($this->access_type === UsmModule::ACCESS_TYPE_YII && is_callable($this->access_admin_check)) {
            return call_user_func($this->access_admin_check);
        } else {
            return (!\Yii::$app->user->isGuest);
        }
    }

    public function setEditMode($value) {
        $this->_editMode = !!$value;
    }

    public function getEditMode() {
        if (!$this->isUserAdmin()) {
            return false;
        }
        return $this->_editMode;
    }

    public function getAdminMenu() {
        return $this->_menu;
    }

    private $_pageUrl = null;

    public function getPageUrl() {
        return $this->_pageUrl;
    }

    public function setPageUrl($val) {
        $this->_pageUrl = $val;
    }

    private $_pluginPageOptions = null;
    public function getAvailablePluginPages() {
        if ($this->_pluginPageOptions === null) {
            $this->_pluginPageOptions = [];
            foreach ($this->_pluginModuleIds as $moduleId) {
                $module = Yii::$app->getModule($moduleId);
                $pages = $module->getPluginPages();
                foreach ($pages as $page) {
                    $this->_pluginPageOptions[] = [
                        'label' => ucfirst($moduleId) . " " . static::PLUGIN_PAGE_LABEL_SEPARATOR . " " . $page['name'],
                        'id' => '/' . $moduleId . '/' . $page['actionId']
                    ];
                }
            }

            foreach ($this->availablePagesForMenu as $page) {
                $this->_pluginPageOptions[] = [
                    'label' => "App / " . $page['name'],
                    'id' => ((substr($page['actionId'], 0, 1) !== '/') ? '/' : '') . $page['actionId']
                ];
            }
            sort($this->_pluginPageOptions);
        }
        return $this->_pluginPageOptions;
    }

    public function reverseLookupPluginPage($url, $fallbackToUrl = true) {
        $pages = $this->getAvailablePluginPages();
        foreach ($pages as $page) {
            if ($page['id'] === $url) {
                return $page['label'];
            }
        }
        return ($fallbackToUrl) ? $url : null;
    }

    public function registerTranslations() {
        Yii::$app->i18n->translations['modules/usm/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en',
            'basePath' => '@usm/messages',
            'fileMap' => [
                'modules/usm/usm' => 'usm.php',
                'modules/usm/error' => 'error.php',
                'modules/usm/menu' => 'menu.php',
                'modules/usm/pages' => 'pages.php',
                'modules/usm/urls' => 'urls.php',
                'modules/usm/textblocks' => 'textblocks.php',
                'modules/usm/files' => 'files.php',
            ],
        ];
    }

    public static function t($category, $message, $params = [], $language = null) {
        return Yii::t('modules/usm/' . $category, $message, $params, $language);
    }

    private $_isToolbarRendered = false;
    public function isToolbarRendered() {
        if (!$this->_isToolbarRendered) {
            $this->_isToolbarRendered = true;
            return false;
        }
        return true;
    }
}
