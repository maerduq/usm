<?php

namespace maerduq\usm\plugins;

class UsmPluginModule extends \yii\base\Module {

    /**
     * Array of objects with the following keys:
     * - `name` - name of the page
     * - `actionId`, id of the action to link to for the page. Id should be relative to the module
     */
    public function getPluginPages() {
        return [];
    }

    /**
     * Array of admin menu items to display in the Admin panel
     * - `label` - label of the menu item (can be html)
     * - `icon` - optional icon of the menu item
     * - `url` - Yii style url of the menu item, relative to the module
     */
    public function getAdminMenuItems() {
        return [];
    }

    /**
     * Array of controller IDs (relative to the module) that contain a sitemap() method
     */
    public function getSitemapControllers() {
        return [];
    }
}
