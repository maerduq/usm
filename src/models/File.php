<?php

namespace maerduq\usm\models;

use maerduq\usm\components\Usm;
use maerduq\usm\UsmModule;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "usm_files".
 *
 * The followings are the available columns in table 'usm_files':
 * @property integer $id
 * @property string $slug
 * @property string $category
 * @property string $name
 * @property int $access
 * @property string $file_name
 * @property string $file_type
 * @property integer $file_size
 * @property string $file_ext
 * @property string $last_accessed_at
 * @property string $created_at
 * @property string $updated_at
 * @property string $filePath
 * @property string $accessBadge
 * @property string $fileReferencingInfo
 * @property string $fullName
 * @property string $fullNameWithCategory
 * @property bool $isImage
 * @property string $downloadLink
 * @property string $downloadUrl
 */
class File extends ActiveRecord {
    public $fullName;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                ],
                'value' => new Expression('UTC_TIMESTAMP()'),
            ]
        ];
    }

    public $file, $prefix = 'usm/';

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return 'usm_files';
    }

    public static function isFileDirectoryWritable() {
        return (is_writable(static::fileDir()));
    }

    public static function getFileSizeLimit($formatted = true) {
        $uploadMaxFilesize = static::sizeToBytes(ini_get('upload_max_filesize'));
        $postMaxSize = static::sizeToBytes(ini_get('post_max_size'));

        $maxSize = min($uploadMaxFilesize, $postMaxSize);
        if ($formatted) {
            return static::bytesToSize($maxSize, 0);
        } else {
            return $maxSize;
        }
    }

    public static function sizeToBytes($str) {
        $str = strtolower($str);
        if (substr($str, -1) === 'g') {
            return ((int) substr($str, 0, -1)) * 1073741824;
        } elseif (substr($str, -1) === 'm') {
            return ((int) substr($str, 0, -1)) * 1048576;
        } elseif (substr($str, -1) === 'k') {
            return ((int) substr($str, 0, -1)) * 1024;
        } else {
            return (int) $str;
        }
    }

    public static function bytesToSize($nrBytes, $nrDecimals = 2) {
        if ($nrBytes < 1000) {
            return $nrBytes . " byte";
        } elseif (($temp = $nrBytes / 1000) < 1000) {
            return \Yii::$app->formatter->asDecimal($temp, $nrDecimals) . " kB";
        } elseif (($temp = $temp / 1000) < 1000) {
            return \Yii::$app->formatter->asDecimal($temp, $nrDecimals) . " MB";
        } else {
            $temp /= 1000;
            return \Yii::$app->formatter->asDecimal($temp, $nrDecimals) . " GB";
        }
    }

    public static function fileDir() {
        switch ($_SERVER['HTTP_HOST']) {
            default:
                $img = \Yii::$app->basePath . "/files/";
        }

        return $img;
    }

    private static $_categories;
    public static function getAvailableCategories() {
        if (static::$_categories === null) {
            static::$_categories = static::find()->andWhere('category is not null')->groupBy('category')->orderBy('category')->select('category')->column();
        }
        return static::$_categories;
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['file_name', 'file_type', 'file_size', 'file_ext', 'access', 'slug', 'file_hash'], 'required'],
            [['file_size', 'access'], 'integer'],
            [['name', 'category'], 'string', 'max' => 60],
            [['file_name'], 'string', 'max' => 120],
            [['file_ext'], 'string', 'max' => 10],
            [['file_type', 'slug'], 'string', 'max' => 255],
            [['file'], 'file', 'maxSize' => static::getFileSizeLimit(false)],
            [['file_name', 'slug'], 'unique'],
            ['name', 'unique', 'targetAttribute' => ['name', 'category', 'file_ext']],
            [['file'], 'required', 'on' => 'new'],
            [['name'], 'required', 'except' => 'new'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => UsmModule::t('usm', 'ID'),
            'category' => UsmModule::t('files', 'Category'),
            'name' => UsmModule::t('usm', 'Name'),
            'fullName' => UsmModule::t('usm', 'Name'),
            'access' => UsmModule::t('usm', 'Access'),
            'accessBadge' => UsmModule::t('usm', 'Access'),
            'file_name' => UsmModule::t('files', 'File Name'),
            'file_type' => UsmModule::t('files', 'File Type'),
            'file_size' => UsmModule::t('files', 'File Size'),
            'file_size_readable' => UsmModule::t('files', 'File Size'),
            'file_ext' => UsmModule::t('files', 'File Extension'),
            'file_hash' => UsmModule::t('files', 'File Hash (md5)'),
            'last_accessed_at' => UsmModule::t('files', 'Last Accessed'),
            'created_at' => UsmModule::t('usm', 'Created at'),
            'updated_at' => UsmModule::t('usm', 'Updated at'),
        ];
    }

    public function beforeValidate() {
        if ($this->slug === null) {
            $this->slug = $this->generateRandomSlug();
        }
        if ($this->category === '') {
            $this->category = null;
        }
        if (is_int($this->name)) {
            $this->name = (string) $this->name;
        }

        $continueValidation = false;
        if (is_object($this->file) && get_class($this->file) == 'yii\web\UploadedFile') {
            $continueValidation = true;
        } else if ((UploadedFile::getInstance($this, 'file') != null)) { //als er een nieuw bestand is
            $this->file = UploadedFile::getInstance($this, 'file');
            $continueValidation = true;
        }

        if (!$continueValidation) {
            return true;
        }

        if (!$this->isFileDirectoryWritable()) {
            $this->addError('file', UsmModule::t('error', 'The directory for USM files ({0}) is not writable! Make sure to make this directory writable to be able to upload (new) files.', [static::fileDir()]));
        }

        // Determine name if no name is set
        if ($this->name === '' || $this->name === null) {
            $str = explode(".", $this->file->name);
            array_pop($str);
            $this->name = implode(".", $str);
        }

        if ($this->file_name !== null) {
            @unlink($this->filePath);
        }

        $this->file_name = $this->generateRandomFileName();
        $this->file_type = $this->file->type;
        $this->file_size = $this->file->size;
        $this->file_ext = $this->file->extension;
        $this->file_hash = hash_file('md5', $this->file->tempName);

        return true;
    }

    public function afterSave($insert, $changedAttributes) {
        if (is_object($this->file) && get_class($this->file) == "yii\web\UploadedFile") {
            $this->file->saveAs($this->filePath);
        }
    }

    public function afterDelete() {
        @unlink($this->filePath);
        return true;
    }

    public function getFullNameWithCategory() {
        if ($this->category === null) {
            return $this->fullName;
        } else {
            return $this->category . '/' . $this->fullName;
        }
    }
    public function getFilePath() {
        if ($this->file_name === null) {
            return false;
        }
        $filePath = static::fileDir() . $this->file_name;
        return $filePath;
    }


    public function getAccessBadge() {
        $accessOptions = Usm::getAccessOptions();
        if (!isset($accessOptions[$this->access])) {
            return null;
        }

        $label = $accessOptions[$this->access];
        $color = Usm::$accessColors[$this->access];

        return Html::tag('span', $label, ['class' => 'badge rounded-pill text-bg-' . $color]);
    }


    public function getFile_size_readable() {
        return static::bytesToSize($this->file_size);
    }

    public function getFileReferencingInfo() {
        return UsmModule::t('files', "In the website <pre>{0}</pre>Somewhere else <pre>{1}</pre>", [$this->downloadUrl, $this->getDownloadUrl(true)]);
    }

    public function getIsImage() {
        return in_array(strtolower($this->file_ext), ['jpg', 'jpeg', 'gif', 'bmp', 'png', 'svg']);
    }

    public function getDownloadLink($options = []) {
        return Html::a($this->fullName, $this->downloadUrl, $options);
    }

    public function getDownloadUrl($fullUrl = false) {
        return Url::to(['/usm/files/download-v2', 'slug' => $this->slug, 'ext' => $this->file_ext], $fullUrl);
    }

    public function generateRandomFileName() {
        do {
            $characters = "0123456789abcdefghijklmnopqrstuvwxyzQWERTYUIOPLKJHGFDSAMNBVCXZ";
            $string = "";
            for ($p = 0; $p < 20; $p++) {
                $string .= $characters[mt_rand(0, strlen($characters) - 1)];
            }
            $newFileName = $this->prefix . $string;
        } while (file_exists(static::fileDir() . $newFileName));
        return $newFileName;
    }

    public function generateRandomSlug() {
        return bin2hex(random_bytes(10));
    }

    /**
     * {@inheritdoc}
     * @return UsmFilesQuery the active query used by this AR class.
     */
    public static function find() {
        return (new FileQuery(get_called_class()))->default();
    }

    /**
     * This is a v1 API. Keep intact
     */
    public static function make($name, $category, $postFile, $prefix = 'usm/', $access = 0) {
        $file = static::findOne(['name' => $name, 'category' => $category]);
        if ($file === null) {
            $file = new static();
            $file->name = $name;
            $file->category = $category;
            $file->prefix = $prefix;
            $file->access = $access;
        }
        $file->file = $postFile;
        if (!$file->save()) {
            throw new \yii\web\HttpException(400, strip_tags(Html::errorSummary($file)));
        }
        return $file;
    }

    /**
     * @return static
     */
    public static function upsert($uploadedFile, $id = null, $name = null, $category = null, $prefix = 'usm/', $access = 0) {
        $newFile = null;
        if ($id !== null) {
            $newFile = static::findOne($id);
        } elseif ($name !== null) {
            $newFile = static::findOne(['name' => $name, 'category' => $category]);
        }

        if ($newFile === null) {
            $newFile = new static();
        }

        $newFile->name = $name;
        $newFile->category = $category;
        $newFile->prefix = $prefix;
        $newFile->access = $access;

        $newFile->file = $uploadedFile;

        if (!$newFile->save()) {
            throw new \yii\web\HttpException(400, strip_tags(Html::errorSummary($newFile)));
        }

        $newFile->touch('updated_at');
        return $newFile;
    }
}
