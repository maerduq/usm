<?php

namespace maerduq\usm\models;

/**
 * This is the ActiveQuery class for [[UsmFiles]].
 *
 * @see File
 */
class FileQuery extends \yii\db\ActiveQuery {
    public static function getFullNameQuery() {
        return "CONCAT(`name`,'.',`file_ext`)";
    }

    public function default() {
        return $this->select(['*', $this->getFullNameQuery() . ' as fullName']);
    }

    /**
     * {@inheritdoc}
     * @return UsmFiles[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UsmFiles|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
}
