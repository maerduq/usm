<?php

namespace maerduq\usm\models;

use maerduq\usm\components\TranslatedActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use maerduq\usm\components\Usm;
use maerduq\usm\UsmModule;

/**
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 */
class Textblock extends TranslatedActiveRecord {

    public static function tableName() {
        return 'usm_textblocks';
    }

    public function translationSettings() {
        return [
            'itemType' => 'textblock',
            'translatedAttributes' => ['text']
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('UTC_TIMESTAMP()'),
            ]
        ];
    }

    public function rules() {
        return [
            [['name'], 'required'],
            [['name'], 'unique'],
            [['text', 'description', 'translations'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => UsmModule::t('usm', 'ID'),
            'name' => UsmModule::t('usm', 'Name'),
            'text' => UsmModule::t('usm', 'Text'),
            'description' => UsmModule::t('usm', 'Description'),
            'created_at' => UsmModule::t('usm', 'Created at'),
            'updated_at' => UsmModule::t('usm', 'Updated at'),
        ];
    }

    public function getStrippedText() {
        return strip_tags($this->text);
    }

    public static function read($name, $replaces = [], $defaultContent = null) {
        if (!is_array($name)) {
            $item = static::findOneOrCreate($name, $defaultContent);
            $text = $item->getTranslation('text');
            if ($text === null) {
                return '';
            }
            return static::evalIt($text, $replaces);
        } else {
            $return = [];
            foreach ($name as $n) {
                $item = static::findOneOrCreate($n);
                $text = $item->getTranslation('text');
                if ($text === null) {
                    $return[$n] = '';
                } else {
                    $return[$n] = static::evalIt($text, $replaces);
                }
            }
            return $return;
        }
    }

    public static function findOneOrCreate($name, $defaultContent = null) {
        $item = static::findOne(['name' => $name]);
        if ($item === null) {
            $item = new static();
            $item->name = $name;
            $item->text = $defaultContent;
            $item->save();
        }
        return $item;
    }

    private static function evalIt($text, $replaces) {
        $text = Usm::evalContent($text);

        foreach ($replaces as $find => $replace) {
            $text = str_replace('{{' . $find . '}}', $replace, $text);
        }

        return $text;
    }

    public static function write($in = [], $value = null, $lang = null) {
        if ($value != null && !is_array($in)) {
            if ($lang === null) {
                $lang = \Yii::$app->language;
            }

            $in = [$in => [$lang => $value]];
        }

        foreach ($in as $name => $list) {
            foreach ($list as $lang => $content) {
                $model = static::findOne(['name' => $name]);
                if ($model === null) {
                    $model = new static();
                    $model->name = $name;
                }

                if ($lang === Usm::getBaseLanguage()) {
                    $model->text = $content;
                    $model->save();
                } else {
                    $model->saveTranslation('text', $lang, $content);
                }
            }
        }
    }

    public function beforeValidate() {
        if ($this->description === '') {
            $this->description = null;
        }

        return parent::beforeValidate();
    }
}
