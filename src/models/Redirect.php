<?php

namespace maerduq\usm\models;

use maerduq\usm\components\Usm;
use maerduq\usm\UsmModule;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @property integer $id
 * @property boolean $active
 * @property string $url
 * @property string $type
 * @property integer $menu_item_id
 * @property string $destination
 * @property boolean $forward
 * @property boolean $generated
 * @property string $created_at
 * @property string $updated_at 
 * @property MenuItem $menuItem
 * @property Page $page
 */
class Redirect extends ActiveRecord {

    public $typeOptions = [];

    public $pageId, $pluginPage, $controllerId;
    public $forwardOptions = [];
    public $activeOptions = [];

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('UTC_TIMESTAMP()'),
            ]
        ];
    }

    public function init() {
        $this->active = 1;
        $this->forward = 0;
        $this->generated = 0;

        $this->forwardOptions = [0 => UsmModule::t('urls', "Endpoint"), 1 => UsmModule::t('urls', "Forward")];
        $this->activeOptions = [1 => UsmModule::t('urls', 'Active'), 0 => UsmModule::t('urls', 'Inactive')];
        $this->typeOptions = [
            'menu_item' => UsmModule::t('usm', 'Menu item'),
            'cms' => UsmModule::t('usm', 'USM page'),
            'plugin' => UsmModule::t('usm', 'Plugin page'),
            'php' => UsmModule::t('usm', 'Yii controller'),
            'link' => UsmModule::t('usm', 'Link')
        ];

        return parent::init();
    }

    public static function tableName() {
        return 'usm_redirects';
    }

    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            [['type'], 'required'],
            [['menu_item_id'], 'integer'],
            [['active', 'forward'], 'boolean'],
            [['url'], 'unique'],
            [['url', 'destination'], 'string', 'max' => 200],
            [['destination', 'pluginPage', 'controllerId', 'pageId'], 'safe'],
            [['type'], 'in', 'range' => array_keys($this->typeOptions)],
            [['url'], 'validateReservedUrls']
        ];
    }

    public function validateReservedUrls($attribute, $params) {
        $path = explode("?", $this->$attribute);
        $path = $path[0];
        if ($path === 'usm' || substr($this->$attribute, 0, 4) === 'usm/') {
            $this->addError($attribute, UsmModule::t('urls', 'Your URL may not start with \'usm\', this is a reserved path.'));
        }
    }

    public function getMenuItem() {
        return $this->hasOne(MenuItem::class, ['id' => 'menu_item_id']);
    }

    public function getPage() {
        return $this->hasOne(Page::class, ['id' => 'destination']);
    }

    public function getActiveBadge() {
        if ($this->active) {
            $label = UsmModule::t('urls', 'Active');
            $color = 'success';
        } else {
            $label = UsmModule::t('urls', 'Inactive');
            $color = 'default';
        }
        return Html::tag('span', $label, ['class' => 'badge rounded-pill text-bg-' . $color]);
    }

    public function getForwardBadge() {
        if ($this->forward) {
            $label = UsmModule::t('urls', 'Forward');
            $color = 'secondary';
        } else {
            $label = UsmModule::t('urls', 'Endpoint');
            $color = 'success';
        }
        return Html::tag('span', $label, ['class' => 'badge rounded-pill text-bg-' . $color]);
    }

    public function getTypeLabel() {
        switch ($this->type) {
            case "cms":
                return UsmModule::t('usm', 'USM page') . ' ' . Html::a($this->page->title, ['pages/update', 'id' => $this->destination, 'return' => Usm::returnUrl()], ['data-pjax' => 0]);
            case "php":
                return UsmModule::t('usm', 'Yii controller') . ' ' . Html::tag('b', $this->destination);
            case "plugin":
                return UsmModule::t('usm', 'Plugin page') . ' ' . Html::tag('b', $this->pluginPage);
            case "menu_item":
                if ($this->menuItem == null) {
                    return "Missing menu item!";
                }
                return UsmModule::t('usm', 'Menu item') . ' ' . Html::a($this->menuItem->title, ['menu/update', 'id' => $this->menu_item_id, 'return' => Usm::returnUrl()], ['data-pjax' => 0]);
            case 'link':
                return UsmModule::t('usm', 'Link') . ' ' . Html::a($this->destination, $this->destination, ['title' => $this->destination], ['data-pjax' => 0]);
        }
    }

    public function getTitle() {
        switch ($this->type) {
            case "menu_item":
                if ($this->menuItem === null) {
                    return null;
                }
                return $this->menuItem->getTranslation('title');
            case "cms":
                return $this->page->getTranslation('title');
            case "php":
                $label = $this->destination;
                $parts = explode("/", $label);
                return ucfirst(end($parts));
            case "plugin":
                $label = $this->pluginPage;
                $parts = explode(UsmModule::PLUGIN_PAGE_LABEL_SEPARATOR, $label);
                $name = (count($parts) > 1) ? $parts[1] : $parts[0];
                return trim($name);
            default:
                return null;
        }
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
            'id' => UsmModule::t('usm', 'ID'),
            'active' => UsmModule::t('urls', 'Active'),
            'url' => UsmModule::t('urls', 'URL'),
            'type' => UsmModule::t('urls', 'Destination type'),
            'forward' => UsmModule::t('urls', 'Behavior'),
            'generated' => UsmModule::t('urls', 'Generated'),
            'destination' => UsmModule::t('usm', 'Hyperlink'),
            'menu_item_id' => UsmModule::t('usm', 'Menu item'),
            'pageId' => UsmModule::t('usm', 'USM Page'),
            'controllerId' => UsmModule::t('usm', 'Yii controller'),
        ];
    }

    public function afterFind() {
        switch ($this->type) {
            case 'cms':
                $this->pageId = $this->destination;
                break;
            case 'plugin':
                $pluginPages = UsmModule::getInstance()->getAvailablePluginPages();
                foreach ($pluginPages as $index => $page) {
                    if ($page['id'] === $this->destination) {
                        $this->pluginPage = $page['label'];
                        break;
                    }
                }
                break;
            case 'php':
                $this->controllerId = $this->destination;
                break;
        }
    }

    public function beforeValidate() {
        if (parent::beforeValidate() === false) {
            return false;
        }

        if (!isset($this->url)) {
            $this->url = '';
        }
        $this->url = Usm::makeUrl($this->url);

        switch ($this->type) {
            case 'menu_item':
                $this->destination = null;
                if ($this->menu_item_id === '' || $this->menu_item_id === null) {
                    $this->addError('menu_item_id', UsmModule::t('urls', 'Menu item is a required field.'));
                }
                break;
            case 'cms':
                $this->menu_item_id = null;
                if ($this->pageId === '' || $this->pageId === null) {
                    $this->addError('pageId', UsmModule::t('urls', 'USM Page is a required field.'));
                }
                $this->destination = $this->pageId;
                break;
            case 'plugin':
                $this->menu_item_id = null;
                if ($this->pluginPage === '' || $this->pluginPage === null) {
                    $this->addError('pluginPage', UsmModule::t('urls', 'Plugin page is a required field'));
                } else {
                    $pluginPages = UsmModule::getInstance()->getAvailablePluginPages();
                    $selectedPage = $pluginPages[$this->pluginPage];
                    $this->destination = $selectedPage['id'];
                }
                break;
            case 'php':
                $this->menu_item_id = null;
                if ($this->controllerId === '' || $this->controllerId === null) {
                    $this->addError('controllerId', UsmModule::t('urls', 'Yii controller is a required field'));
                } else {
                    $this->destination = $this->controllerId;
                }
                break;
            case 'link':
                $this->menu_item_id = null;
                if ($this->destination === '' || $this->destination === null) {
                    $this->addError('destination', UsmModule::t('urls', 'Hyperlink is a required field'));
                }
                break;
        }

        return true;
    }

    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->type != 'menu_item') {
            $this->menu_item_id = null;
        }

        if ($this->type == 'link') {
            $this->forward = 1;
        }

        return true;
    }

    public function getPluginPageOptions() {
        return ArrayHelper::getColumn(UsmModule::getInstance()->getAvailablePluginPages(), 'label');
    }
}
