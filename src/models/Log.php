<?php

namespace maerduq\usm\models;

use maerduq\usm\UsmModule;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "usm_log".
 *
 * @property int $id
 * @property string|null $ip
 * @property string|null $action
 * @property string|null $info
 * @property int|null $attempt
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Log extends ActiveRecord {

    const ACTION_LOGIN_SUCCESS = 'login-success';
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'usm_log';
    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('UTC_TIMESTAMP()'),
            ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['attempt'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['ip', 'action', 'info'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => UsmModule::t('usm', 'ID'),
            'ip' => UsmModule::t('usm', 'IP'),
            'action' => UsmModule::t('usm', 'Action'),
            'info' => UsmModule::t('usm', 'Info'),
            'attempt' => UsmModule::t('usm', 'Attempt'),
            'created_at' => UsmModule::t('usm', 'Created at'),
            'updated_at' => UsmModule::t('usm', 'Updated at'),
        ];
    }

    public static function add(string $action) {
        $newLogRecord = new self();
        $newLogRecord->ip = \Yii::$app->request->userIP;
        $newLogRecord->action = $action;
        return $newLogRecord->save();
    }
}
