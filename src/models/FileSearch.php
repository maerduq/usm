<?php

namespace maerduq\usm\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use maerduq\usm\models\File;
use yii\db\Expression;

/**
 * FileSearch represents the model behind the search form of `maerduq\usm\models\File`.
 */
class FileSearch extends File {
    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'access', 'file_size'], 'integer'],
            [['category', 'fullName', 'name', 'file_name', 'file_type', 'file_ext', 'last_accessed_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = File::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'category' => SORT_ASC,
                    'fullName' => SORT_ASC,
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ]
        ]);

        $dataProvider->sort->attributes['fullName'] = [
            'asc' => ['fullName' => SORT_ASC],
            'desc' => ['fullName' => SORT_DESC],
            'default' => SORT_ASC,
        ];

        $dataProvider->sort->attributes['file_size']['default'] = SORT_DESC;
        $dataProvider->sort->attributes['last_accessed_at']['default'] = SORT_DESC;
        $dataProvider->sort->attributes['created_at']['default'] = SORT_DESC;

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'access' => $this->access,
            'file_size' => $this->file_size,
            'last_accessed_at' => $this->last_accessed_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', new Expression(FileQuery::getFullNameQuery()), $this->fullName])
            ->andFilterWhere(['like', 'file_name', $this->file_name])
            ->andFilterWhere(['like', 'file_type', $this->file_type])
            ->andFilterWhere(['like', 'file_ext', $this->file_ext]);

        return $dataProvider;
    }
}
