<?php

namespace maerduq\usm\models;

use maerduq\usm\components\TranslatedActiveRecord;
use maerduq\usm\components\Usm;
use maerduq\usm\UsmModule;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

/**
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property integer $access
 * @property boolean $rawEdit
 * @property boolean $wysiwyg
 * @property string $style
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property MenuItem[] $menuItems
 */
class Page extends TranslatedActiveRecord {
    public $rawEdit, $updatedByCascadingMenuItemAccess = false;
    public $styleOptions;

    public function translationSettings() {
        return [
            'itemType' => 'page',
            'translatedAttributes' => ['title', 'content']
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('UTC_TIMESTAMP()'),
            ]
        ];
    }


    public function init() {
        $this->wysiwyg = 1;

        $this->styleOptions = [
            "container" => UsmModule::t('pages', "In standard layout"),
            "plain" => UsmModule::t('pages', "In simple layout"),
            "empty" => UsmModule::t('pages', "Without layout"),
        ];

        return parent::init();
    }

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return 'usm_pages';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['title', 'access'], 'required'],
            [['wysiwyg', 'access'], 'integer'],
            [['title', 'style'], 'string', 'max' => 100],
            [['content', 'translations', 'rawEdit'], 'safe'],
            [['access'], 'in', 'range' => array_keys(Usm::getAccessOptions())],
        ];
    }

    public function getMenuItems() {
        return $this->hasMany(MenuItem::class, ['page_id' => 'id'])
            ->where(MenuItem::tableName() . '.type="cms"');
    }

    public function getRedirects() {
        return $this->hasMany(Redirect::class, ['destination' => 'id'])
            ->where('type = "cms" AND forward = 0');
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
            'id' => UsmModule::t('usm', 'ID'),
            'title' => UsmModule::t('pages', 'Page title'),
            'content' => UsmModule::t('pages', 'Page content'),
            'wysiwyg' => UsmModule::t('pages', 'WYSIWYG editing'),
            'style' => UsmModule::t('pages', 'Presenting style'),
            'access' => UsmModule::t('usm', 'Access level'),
            'rawEdit' => UsmModule::t('pages', 'Edit page in raw HTML'),
        ];
    }

    public function getAccessBadge() {
        $accessOptions = Usm::getAccessOptions();
        if (!isset($accessOptions[$this->access])) {
            return null;
        }

        $label = $accessOptions[$this->access];
        $color = Usm::$accessColors[$this->access];

        return Html::tag('span', $label, ['class' => 'badge rounded-pill text-bg-' . $color]);
    }

    public function beforeValidate() {
        if ($this->rawEdit !== null) {
            $this->wysiwyg = ($this->rawEdit) ? 0 : 1;
        }
        return parent::beforeValidate();
    }

    public function beforeDelete() {
        if (count($this->menuItems) > 0) {
            \Yii::$app->session->setFlash('error', UsmModule::t('pages', 'Cannot delete page, it is still used in the menu.'));
            return false;
        }

        return true;
    }

    public function afterFind() {
        $this->rawEdit = ($this->wysiwyg === 1) ? 0 : 1;
    }

    public function afterSave($insert, $changedAttributes) {
        if (!$this->updatedByCascadingMenuItemAccess && isset($changedAttributes['access'])) {
            foreach ($this->menuItems as $m) {
                $m->access = $this->access;
                $m->updatedByCascadingPageAccess = true;
                $m->save();
            }
        }
        return parent::afterSave($insert, $changedAttributes);
    }
}
