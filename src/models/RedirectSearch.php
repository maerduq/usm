<?php

namespace maerduq\usm\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use maerduq\usm\models\Redirect;

/**
 * RedirectSearch represents the model behind the search form of `maerduq\usm\models\Redirect`.
 */
class RedirectSearch extends Redirect {
    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'active', 'menu_item_id', 'forward', 'generated'], 'integer'],
            [['url', 'type', 'destination', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    public function init() {
        parent::init();

        $this->forward = null;
        $this->generated = null;
        $this->active = null;
    }


    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Redirect::find();


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['active' => SORT_DESC, 'url' => SORT_ASC],
                'attributes' => ['url', 'type', 'generated', 'destination', 'active', 'forward', 'redirect'],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'active' => $this->active,
            'menu_item_id' => $this->menu_item_id,
            'forward' => $this->forward,
            'type' => $this->type,
            'generated' => $this->generated,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'destination', $this->destination]);

        return $dataProvider;
    }
}
