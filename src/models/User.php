<?php

namespace maerduq\usm\models;

use maerduq\usm\components\UserStore;
use maerduq\usm\UsmModule;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface {
    public $id;
    public $username;
    public $password;

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id) {
        return UserStore::getInstance()->findIdentity($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return UserStore::getInstance()->findByUsername($username);
    }

    /**
     * Finds admin user
     *
     * @return static|null
     */
    public static function getAdmin() {
        return static::findByUsername(UserStore::ADMIN_USERNAME);
    }

    /**
     * {@inheritdoc}
     */
    public function getId() {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey() {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey) {
        return null;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password) {
        switch (UsmModule::getInstance()->access_type) {
            case UsmModule::ACCESS_TYPE_USM:
                return ($password === $this->password);
            case UsmModule::ACCESS_TYPE_USM_SECURE:
                try {
                    $passwordOk = (\Yii::$app->getSecurity()->validatePassword($password, $this->password));
                } catch (\yii\base\InvalidArgumentException $e) {
                    $passwordOk = false;
                }
                return $passwordOk;
            default:
                return false;
        }
    }
}
