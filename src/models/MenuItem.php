<?php

namespace maerduq\usm\models;

use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use maerduq\usm\components\Usm;
use maerduq\usm\components\TranslatedActiveRecord;
use maerduq\usm\UsmModule;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "menuitems".
 *
 * The followings are the available columns in table 'menuitems':
 * @property integer $id
 * @property integer $parent_id
 * @property integer $position
 * @property string $title
 * @property string $alias
 * @property string $type
 * @property integer $page_id
 * @property string $url
 * @property boolean $visible
 * @property integer $access
 * @property string $created_at
 * @property string $updated_at
 * @property MenuItem $parent
 * @property Page $page
 * @property Redirect $redirect
 * @property MenuItem[] $children
 */
class MenuItem extends TranslatedActiveRecord {

    public $typeOptions = [];
    public $pluginPage, $controllerId;
    public $updatedByCascadingPageAccess = false;

    public function translationSettings() {
        return [
            'itemType' => 'menu_item',
            'translatedAttributes' => ['title']
        ];
    }

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return 'usm_menu_items';
    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('UTC_TIMESTAMP()'),
            ]
        ];
    }

    public function init() {
        $this->visible = 1;

        $this->typeOptions = [
            'empty' => UsmModule::t('usm', 'Empty menu item'),
            'cms' => UsmModule::t('usm', 'USM page'),
            'plugin' => UsmModule::t('usm', 'Plugin page'),
            'php' => UsmModule::t('usm', 'Yii controller'),
            'link' => UsmModule::t('usm', 'Link')
        ];

        return parent::init();
    }

    public function rules() {
        return [
            [['title', 'type', 'access'], 'required'],
            [['alias'], 'unique'],
            [['parent_id', 'position', 'access'], 'integer'],
            [['visible'], 'boolean'],
            [['title', 'alias'], 'string', 'max' => 100],
            [['url'], 'string', 'max' => 200],
            [['page_id', 'translations', 'pluginPage', 'controllerId'], 'safe'],
            [['type'], 'in', 'range' => array_keys($this->typeOptions)],
            [['access'], 'in', 'range' => array_keys(Usm::getAccessOptions())],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => UsmModule::t('usm', 'ID'),
            'visible' => UsmModule::t('usm', 'Visible'),
            'position' => UsmModule::t('menu', 'Position'),
            'alias' => UsmModule::t('menu', 'URL alias'),
            'title' => UsmModule::t('menu', 'Menu item title'),
            'type' => UsmModule::t('menu', 'Destination type'),
            'page_id' => UsmModule::t('usm', 'Page'),
            'url' => UsmModule::t('menu', 'URL'),
            'parent_id' => UsmModule::t('menu', 'Parent'),
            'access' => UsmModule::t('usm', 'Access level'),
            'controllerId' => UsmModule::t('menu', 'Yii controller'),
        ];
    }

    public function afterFind() {
        switch ($this->type) {
            case 'plugin':
                $pluginPages = UsmModule::getInstance()->getAvailablePluginPages();
                foreach ($pluginPages as $index => $page) {
                    if ($page['id'] === $this->url) {
                        $this->pluginPage = $index;
                        break;
                    }
                }
                break;
            case 'php':
                $this->controllerId = $this->url;
                break;
            case 'cms':
                $this->access = $this->page->access;
                break;
        }
    }

    public function beforeValidate() {
        if (parent::beforeValidate() === false) {
            return false;
        }

        if ($this->alias === null || $this->alias === '') {
            $this->alias = $this->title;
        }
        $this->alias = Usm::makeAlias($this->alias);

        switch ($this->type) {
            case 'empty':
                $this->page_id = null;
                $this->url = null;
                break;
            case 'cms':
                $this->url = null;

                if ($this->page_id == null) {
                    $newPage = new Page();
                    $newPage->title = $this->title;
                    $newPage->content = "<p>Page content coming soon</p>";
                    $newPage->access = $this->access;
                    $newPage->save();
                    $this->page_id = $newPage->id;
                }
                break;
            case 'plugin':
                $this->page_id = null;
                $this->url = null;
                if ($this->pluginPage === '' || $this->pluginPage === null) {
                    $this->addError('pluginPage', UsmModule::t('menu', 'Plugin page is a required field'));
                } else {
                    $pluginPages = UsmModule::getInstance()->getAvailablePluginPages();
                    $selectedPage = $pluginPages[$this->pluginPage];
                    $this->url = $selectedPage['id'];
                }
                break;
            case 'php':
                $this->page_id = null;
                if ($this->controllerId === '' || $this->controllerId === null) {
                    $this->addError('controllerId', UsmModule::t('menu', 'Yii controller is a required field'));
                } else {
                    $this->url = $this->controllerId;
                }
                break;
            case 'link':
                $this->page_id = null;
                if ($this->url === '' || $this->url === null) {
                    $this->addError('url', UsmModule::t('menu', 'URL is a required field'));
                }
                break;
            default:
                $this->page_id = null;
        }

        return true;
    }

    public function afterValidate() {
        if ($this->isNewRecord) {
            $highestPosition = MenuItem::find()
                ->select('position')
                ->where('parent_id IS NULL')
                ->orderBy('position DESC')
                ->one();

            $this->position = ($highestPosition == null) ? 1 : $highestPosition->position + 1;
        }
        return parent::afterValidate();
    }

    public function afterSave($insert, $changedAttributes) {
        if ($this->type === 'cms' && !$this->updatedByCascadingPageAccess && ($insert || (!$insert && isset($changedAttributes['access'])))) {
            $this->page->access = $this->access;
            $this->page->updatedByCascadingMenuItemAccess = true;
            $this->page->save();
        }

        $this->updateRedirect($this);
        return parent::afterSave($insert, $changedAttributes);
    }

    public function getPluginPageOptions() {
        return ArrayHelper::getColumn(UsmModule::getInstance()->getAvailablePluginPages(), 'label');
    }

    public function getParent() {
        return $this->hasOne(MenuItem::class, ['id' => 'parent_id']);
    }

    public function getPage() {
        return $this->hasOne(Page::class, ['id' => 'page_id']);
    }

    public function getRedirect() {
        return $this->hasOne(Redirect::class, ['menu_item_id' => 'id'])
            ->where(['generated' => 1]);
    }

    public function getChildren() {
        return $this->hasMany(MenuItem::class, ['parent_id' => 'id']);
    }

    private function updateRedirect(&$item) {
        $r = $item->redirect;
        if ($item->type == 'empty') {
            if ($r != null) {
                $r->delete();
            }
        } else {
            if ($r == null) {
                $r = new Redirect();
                $r->type = 'menu_item';
                $r->menu_item_id = $item->id;
            }
            $r->active = 1;
            $r->forward = 0;
            $r->generated = 1;
            $r->url = ($item->parent_id != null) ? $item->parent->alias . "/" . $item->alias : $item->alias;
            $postfix = 1;
            while (!$r->save() && count($r->getErrors('url')) > 0) {
                $r->url = (($postfix == 1) ? $r->url : substr($r->url, 0, -1 * (strlen($postfix) + 1))) . '-' . ($postfix++);
            }
        }

        if (count($item->children) > 0) {
            foreach ($item->children as $c) {
                $this->updateRedirect($c);
            }
        }
    }
}
