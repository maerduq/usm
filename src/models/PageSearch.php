<?php

namespace maerduq\usm\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use maerduq\usm\models\Page;

/**
 * PageSearch represents the model behind the search form of `maerduq\usm\models\Page`.
 */
class PageSearch extends Page {
    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'access', 'wysiwyg'], 'integer'],
            [['title', 'content', 'style', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    public function init() {
        $this->wysiwyg = null;
        $this->access = null;
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Page::find()->with(['menuItems', 'redirects']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'title' => SORT_ASC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'access' => $this->access,
            'wysiwyg' => $this->wysiwyg,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'style', $this->style]);

        return $dataProvider;
    }
}
