<?php

namespace usm\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\ArrayHelper;

/**
 * Test your mail setup.
 */
class MailTestController extends Controller {

    /**
     * Retrieve your mail configuration.
     */
    public function actionIndex() {
        $this->_printInfo("@app/config/web.php", "Web");
        echo PHP_EOL;

        $this->_printInfo("@app/config/console.php", "Console");

        return ExitCode::OK;
    }

    /**
     * Send a test mail.
     */
    public function actionSend($to, $body = 'Test body', $subject = 'Test subject') {

        $resultOk = \Yii::$app->mailer->compose()
            ->setTo($to)
            ->setFrom([$_ENV['SENDER_EMAIL'] => $_ENV['SENDER_NAME']])
            ->setSubject($subject)
            ->setTextBody(strip_tags($body))
            ->setHtmlBody($body)
            ->send();

        return ($resultOk) ? ExitCode::OK : ExitCode::UNSPECIFIED_ERROR;
    }

    private function _printInfo($configFilePath, $configName = "") {
        echo "Environment variables" . PHP_EOL;
        echo "- SENDER_EMAIL: " . ArrayHelper::getValue($_ENV, 'SENDER_EMAIL') . PHP_EOL;
        echo "- SENDER_NAME: " . ArrayHelper::getValue($_ENV, 'SENDER_NAME') . PHP_EOL;
        echo PHP_EOL;

        echo "{$configName} config" . PHP_EOL;
        $consoleFile = \Yii::getAlias($configFilePath);
        if (!file_exists($consoleFile)) {
            echo "! Config file not found" . PHP_EOL;
            return;
        }

        $consoleConfig = require $consoleFile;
        if (!isset($consoleConfig['components']) || !isset($consoleConfig['components']['mailer'])) {
            echo "! No mailer component found" . PHP_EOL;
            return;
        }

        $mailerConfig = $consoleConfig['components']['mailer'];

        echo "- Class: " . ArrayHelper::getValue($mailerConfig, 'class') . PHP_EOL;
        echo "- Transport DSN: " . ArrayHelper::getValue($mailerConfig, 'transport.dsn') . PHP_EOL;
        echo "- View path: " . ArrayHelper::getValue($mailerConfig, 'viewPath') . PHP_EOL;
        echo "- Use file transport: " . ((ArrayHelper::getValue($mailerConfig, 'useFileTransport')) ? 'TRUE' : 'FALSE') . PHP_EOL;
    }
}
