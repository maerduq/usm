<?php

namespace usm\commands;

use yii\console\Controller;
use yii\console\ExitCode;

class PwtoolController extends Controller {

    public function actionHash($pw) {
        echo \Yii::$app->getSecurity()->generatePasswordHash($pw) . PHP_EOL;

        return ExitCode::OK;
    }
}
